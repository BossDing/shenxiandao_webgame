import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import org.yunai.swjg.server.module.item.ItemDef;
import org.yunai.swjg.server.module.item.template.ItemTemplate;
import org.yunai.yfserver.util.StringUtils;

import java.util.Map;

/**
 * 道具模版处理
 * User: yunai
 * Date: 13-6-3
 * Time: 上午1:07
 */
public class ItemTemplateHtmlHandler {

    public static class TempItem {
        private String name;
        private String pro;
        private String level;
        private String cls;
        private String clc;
        private String pic;

        private String getName() {
            return name;
        }

        private void setName(String name) {
            this.name = name;
        }

        private String getPro() {
            return pro;
        }

        private void setPro(String pro) {
            this.pro = pro;
        }

        private String getLevel() {
            return level;
        }

        private void setLevel(String level) {
            this.level = level;
        }

        private String getCls() {
            return cls;
        }

        private void setCls(String cls) {
            this.cls = cls;
        }

        private String getClc() {
            return clc;
        }

        private void setClc(String clc) {
            this.clc = clc;
        }

        private String getPic() {
            return pic;
        }

        private void setPic(String pic) {
            this.pic = pic;
        }
    }

    public static class Temp {
        private TypeReference<Map<String, TempItem>> gvbhjnngfdew34drftvb;
        private Map<String, TempItem> sdf345g1df0235235;
        private Map<String, TempItem> vwsq12sd7ft8bynoim099;
        private Map<String, String> hotkw;

        public TypeReference<Map<String, TempItem>> getGvbhjnngfdew34drftvb() {
            return gvbhjnngfdew34drftvb;
        }

        public void setGvbhjnngfdew34drftvb(TypeReference<Map<String, TempItem>> gvbhjnngfdew34drftvb) {
            this.gvbhjnngfdew34drftvb = gvbhjnngfdew34drftvb;
        }

        public Map<String, TempItem> getSdf345g1df0235235() {
            return sdf345g1df0235235;
        }

        public void setSdf345g1df0235235(Map<String, TempItem> sdf345g1df0235235) {
            this.sdf345g1df0235235 = sdf345g1df0235235;
        }

        public Map<String, TempItem> getVwsq12sd7ft8bynoim099() {
            return vwsq12sd7ft8bynoim099;
        }

        public void setVwsq12sd7ft8bynoim099(Map<String, TempItem> vwsq12sd7ft8bynoim099) {
            this.vwsq12sd7ft8bynoim099 = vwsq12sd7ft8bynoim099;
        }

        public Map<String, String> getHotkw() {
            return hotkw;
        }

        public void setHotkw(Map<String, String> hotkw) {
            this.hotkw = hotkw;
        }
    }

    private static final String HTML = "{gvbhjnngfdew34drftvb:{\"1\":{\"name\":\"逍遥弓\",\"pro\":\"飞羽\",\"level\":\"20\",\"cls\":\"降魔弓,玄天石,不醉石,念珠\",\"clc\":\"1,27,9,27\",\"pic\":\"1.jpg\"},\"2\":{\"name\":\"逍遥剑\",\"pro\":\"剑灵\",\"level\":\"20\",\"cls\":\"降魔剑,玄天石,不醉石,念珠\",\"clc\":\"1,27,9,27\",\"pic\":\"2.jpg\"},\"3\":{\"name\":\"逍遥戟\",\"pro\":\"将星\",\"level\":\"20\",\"cls\":\"降魔锤,玄天石,不醉石,念珠\",\"clc\":\"1,27,9,27\",\"pic\":\"3.jpg\"},\"4\":{\"name\":\"逍遥拳套\",\"pro\":\"武圣\",\"level\":\"20\",\"cls\":\"降魔拳套,玄天石,不醉石,念珠\",\"clc\":\"1,27,9,27\",\"pic\":\"4.jpg\"},\"5\":{\"name\":\"逍遥杖\",\"pro\":\"术士,方士\",\"level\":\"20\",\"cls\":\"降魔杖,玄天石,不醉石,念珠\",\"clc\":\"1,27,9,27\",\"pic\":\"5.jpg\"},\"6\":{\"name\":\"逍遥战魂\",\"pro\":\"飞羽,剑灵,将星,武圣\",\"level\":\"20\",\"cls\":\"降魔战魂,玄天石,不醉石,念珠\",\"clc\":\"1,27,36,27\",\"pic\":\"6.jpg\"},\"7\":{\"name\":\"逍遥元神\",\"pro\":\"术士,方士\",\"level\":\"20\",\"cls\":\"降魔元神,玄天石,不醉石,念珠\",\"clc\":\"1,27,36,27\",\"pic\":\"7.jpg\"},\"8\":{\"name\":\"逍遥法袍\",\"pro\":\"通用\",\"level\":\"20\",\"cls\":\"降魔法袍,绿珠,蓝宝石,紫幽羽\",\"clc\":\"1,9,18,3\",\"pic\":\"8.jpg\"},\"9\":{\"name\":\"逍遥护符\",\"pro\":\"通用\",\"level\":\"20\",\"cls\":\"降魔护符,绿珠,蓝宝石,紫幽羽\",\"clc\":\"1,9,18,3\",\"pic\":\"9.jpg\"},\"10\":{\"name\":\"逍遥法冠\",\"pro\":\"通用\",\"level\":\"20\",\"cls\":\"降魔法冠,绿珠,蓝宝石,紫幽羽\",\"clc\":\"1,9,18,3\",\"pic\":\"10.jpg\"},\"11\":{\"name\":\"逍遥战靴\",\"pro\":\"通用\",\"level\":\"20\",\"cls\":\"降魔战靴,绿珠,蓝宝石,紫幽羽\",\"clc\":\"1,9,18,3\",\"pic\":\"11.jpg\"},\"12\":{\"name\":\"朱雀弓\",\"pro\":\"飞羽\",\"level\":\"40\",\"cls\":\"逍遥弓,玄阴石,魔木鼎,炎石\",\"clc\":\"1,30,20,60\",\"pic\":\"12.jpg\"},\"13\":{\"name\":\"朱雀剑\",\"pro\":\"剑灵\",\"level\":\"40\",\"cls\":\"逍遥剑,玄阴石,魔木鼎,炎石\",\"clc\":\"1,30,20,60\",\"pic\":\"13.jpg\"},\"14\":{\"name\":\"朱雀枪\",\"pro\":\"将星\",\"level\":\"40\",\"cls\":\"逍遥戟,玄阴石,魔木鼎,炎石\",\"clc\":\"1,30,20,60\",\"pic\":\"14.jpg\"},\"15\":{\"name\":\"朱雀拳套\",\"pro\":\"武圣\",\"level\":\"40\",\"cls\":\"逍遥拳套,玄阴石,魔木鼎,炎石\",\"clc\":\"1,30,20,60\",\"pic\":\"15.jpg\"},\"16\":{\"name\":\"朱雀杖\",\"pro\":\"术士,方士\",\"level\":\"40\",\"cls\":\"逍遥杖,玄阴石,魔木鼎,炎石\",\"clc\":\"1,30,20,60\",\"pic\":\"16.jpg\"},\"17\":{\"name\":\"朱雀战魂\",\"pro\":\"飞羽,剑灵,将星,武圣\",\"level\":\"40\",\"cls\":\"逍遥战魂,玄阴石,魔木鼎,炎石\",\"clc\":\"1,30,20,60\",\"pic\":\"17.jpg\"},\"18\":{\"name\":\"朱雀元神\",\"pro\":\"术士,方士\",\"level\":\"40\",\"cls\":\"逍遥元神,玄阴石,魔木鼎,炎石\",\"clc\":\"1,30,20,60\",\"pic\":\"18.jpg\"},\"19\":{\"name\":\"朱雀法袍\",\"pro\":\"通用\",\"level\":\"40\",\"cls\":\"逍遥法袍,凤凰羽,血莲花,魔木鼎\",\"clc\":\"1,20,25,20\",\"pic\":\"19.jpg\"},\"20\":{\"name\":\"朱雀护符\",\"pro\":\"通用\",\"level\":\"40\",\"cls\":\"逍遥护符,凤凰羽,血莲花,魔木鼎\",\"clc\":\"1,20,25,20\",\"pic\":\"20.jpg\"},\"21\":{\"name\":\"朱雀法冠\",\"pro\":\"通用\",\"level\":\"40\",\"cls\":\"逍遥法冠,凤凰羽,血莲花,魔木鼎\",\"clc\":\"1,20,25,20\",\"pic\":\"21.jpg\"},\"22\":{\"name\":\"朱雀战靴\",\"pro\":\"通用\",\"level\":\"40\",\"cls\":\"逍遥战靴,凤凰羽,血莲花,魔木鼎\",\"clc\":\"1,20,25,20\",\"pic\":\"22.jpg\"},\"23\":{\"name\":\"玄奇弓\",\"pro\":\"飞羽\",\"level\":\"60\",\"cls\":\"朱雀弓,魔玉,天雷珠,至尊鼎\",\"clc\":\"1,15,5,60\",\"pic\":\"23.jpg\"},\"24\":{\"name\":\"玄奇剑\",\"pro\":\"剑灵\",\"level\":\"60\",\"cls\":\"朱雀剑,魔玉,天雷珠,至尊鼎\",\"clc\":\"1,15,5,61\",\"pic\":\"24.jpg\"},\"25\":{\"name\":\"玄奇棍\",\"pro\":\"将星\",\"level\":\"60\",\"cls\":\"朱雀枪,魔玉,天雷珠,至尊鼎\",\"clc\":\"1,15,5,62\",\"pic\":\"25.jpg\"},\"26\":{\"name\":\"玄奇拳套\",\"pro\":\"武圣\",\"level\":\"60\",\"cls\":\"朱雀拳套,魔玉,天雷珠,至尊鼎\",\"clc\":\"1,15,5,63\",\"pic\":\"26.jpg\"},\"27\":{\"name\":\"玄奇杖\",\"pro\":\"术士,方士\",\"level\":\"60\",\"cls\":\"朱雀杖,魔玉,天雷珠,至尊鼎\",\"clc\":\"1,15,5,64\",\"pic\":\"27.jpg\"},\"28\":{\"name\":\"玄奇战魂\",\"pro\":\"飞羽,剑灵,将星,武圣\",\"level\":\"60\",\"cls\":\"朱雀战魂,魔玉,天雷珠,至尊鼎\",\"clc\":\"1,15,5,60\",\"pic\":\"28.jpg\"},\"29\":{\"name\":\"玄奇元神\",\"pro\":\"术士,方士\",\"level\":\"60\",\"cls\":\"朱雀元神,魔玉,天雷珠,至尊鼎\",\"clc\":\"1,15,5,60\",\"pic\":\"29.jpg\"},\"30\":{\"name\":\"玄奇法袍\",\"pro\":\"通用\",\"level\":\"60\",\"cls\":\"朱雀法袍,天雷珠,至尊鼎,地府风灯\",\"clc\":\"1,5,60,75\",\"pic\":\"30.jpg\"},\"31\":{\"name\":\"玄奇护符\",\"pro\":\"通用\",\"level\":\"60\",\"cls\":\"朱雀护符,天雷珠,至尊鼎,地府风灯\",\"clc\":\"1,5,60,75\",\"pic\":\"31.jpg\"},\"32\":{\"name\":\"玄奇法冠\",\"pro\":\"通用\",\"level\":\"60\",\"cls\":\"朱雀法冠,天雷珠,至尊鼎,地府风灯\",\"clc\":\"1,5,60,75\",\"pic\":\"32.jpg\"},\"33\":{\"name\":\"玄奇战靴\",\"pro\":\"通用\",\"level\":\"60\",\"cls\":\"朱雀战靴,天雷珠,至尊鼎,地府风灯\",\"clc\":\"1,5,60,75\",\"pic\":\"33.jpg\"},\"34\":{\"name\":\"倚天弓\",\"pro\":\"飞羽\",\"level\":\"70\",\"cls\":\"玄奇弓,陨石,狼尾草,天心花\",\"clc\":\"1,60,60,30\",\"pic\":\"34.jpg\"},\"35\":{\"name\":\"倚天剑\",\"pro\":\"剑灵\",\"level\":\"70\",\"cls\":\"玄奇剑,陨石,狼尾草,天心花\",\"clc\":\"1,60,60,30\",\"pic\":\"35.jpg\"},\"36\":{\"name\":\"倚天大刀\",\"pro\":\"将星\",\"level\":\"70\",\"cls\":\"玄奇棍,陨石,狼尾草,天心花\",\"clc\":\"1,60,60,30\",\"pic\":\"36.jpg\"},\"37\":{\"name\":\"倚天拳套\",\"pro\":\"武圣\",\"level\":\"70\",\"cls\":\"玄奇拳套,陨石,狼尾草,天心花\",\"clc\":\"1,60,60,30\",\"pic\":\"37.jpg\"},\"38\":{\"name\":\"倚天杖\",\"pro\":\"术士,方士\",\"level\":\"70\",\"cls\":\"玄奇杖,陨石,狼尾草,天心花\",\"clc\":\"1,60,60,30\",\"pic\":\"38.jpg\"},\"39\":{\"name\":\"倚天战魂\",\"pro\":\"飞羽,剑灵,将星,武圣\",\"level\":\"70\",\"cls\":\"玄奇战魂,狼尾草,鬼树枝,天心花\",\"clc\":\"1,60,60,30\",\"pic\":\"39.jpg\"},\"40\":{\"name\":\"倚天元神\",\"pro\":\"术士,方士\",\"level\":\"70\",\"cls\":\"玄奇元神,狼尾草,鬼树枝,天心花\",\"clc\":\"1,60,60,30\",\"pic\":\"40.jpg\"},\"41\":{\"name\":\"倚天法袍\",\"pro\":\"通用\",\"level\":\"70\",\"cls\":\"玄奇法袍,陨石,狼尾草,天心花\",\"clc\":\"1,60,60,30\",\"pic\":\"41.jpg\"},\"42\":{\"name\":\"倚天护符\",\"pro\":\"通用\",\"level\":\"70\",\"cls\":\"玄奇护符,陨石,狼尾草,天心花\",\"clc\":\"1,60,60,30\",\"pic\":\"42.jpg\"},\"43\":{\"name\":\"倚天法冠\",\"pro\":\"通用\",\"level\":\"70\",\"cls\":\"玄奇法冠,陨石,狼尾草,天心花\",\"clc\":\"1,60,60,30\",\"pic\":\"43.jpg\"},\"44\":{\"name\":\"倚天战靴\",\"pro\":\"通用\",\"level\":\"70\",\"cls\":\"玄奇战靴,陨石,狼尾草,天心花\",\"clc\":\"1,60,60,30\",\"pic\":\"44.jpg\"},\"45\":{\"name\":\"赤霄弓\",\"pro\":\"飞羽\",\"level\":\"80\",\"cls\":\"倚天弓,灵果,麒麟角,紫火花\",\"clc\":\"1,25,60,30\",\"pic\":\"45.jpg\"},\"46\":{\"name\":\"赤霄剑\",\"pro\":\"剑灵\",\"level\":\"80\",\"cls\":\"倚天剑,灵果,麒麟角,紫火花\",\"clc\":\"1,25,60,30\",\"pic\":\"46.jpg\"},\"47\":{\"name\":\"赤霄斧\",\"pro\":\"将星\",\"level\":\"80\",\"cls\":\"倚天大刀,灵果,麒麟角,紫火花\",\"clc\":\"1,25,60,30\",\"pic\":\"47.jpg\"},\"48\":{\"name\":\"赤霄拳套\",\"pro\":\"武圣\",\"level\":\"80\",\"cls\":\"倚天拳套,灵果,麒麟角,紫火花\",\"clc\":\"1,25,60,30\",\"pic\":\"48.jpg\"},\"49\":{\"name\":\"赤霄杖\",\"pro\":\"术士,方士\",\"level\":\"80\",\"cls\":\"倚天杖,灵果,麒麟角,紫火花\",\"clc\":\"1,25,60,30\",\"pic\":\"49.jpg\"},\"50\":{\"name\":\"赤霄战魂\",\"pro\":\"飞羽,剑灵,将星,武圣\",\"level\":\"80\",\"cls\":\"倚天战魂,黑流玉,紫火花,九宫瓶\",\"clc\":\"1,30,30,90\",\"pic\":\"50.jpg\"},\"51\":{\"name\":\"赤霄元神\",\"pro\":\"术士,方士\",\"level\":\"80\",\"cls\":\"倚天元神,黑流玉,紫火花,九宫瓶\",\"clc\":\"1,30,30,90\",\"pic\":\"51.jpg\"},\"52\":{\"name\":\"赤霄法袍\",\"pro\":\"通用\",\"level\":\"80\",\"cls\":\"倚天法袍,灵果,九宫瓶,紫火花\",\"clc\":\"1,25,72,30\",\"pic\":\"52.jpg\"},\"53\":{\"name\":\"赤霄护符\",\"pro\":\"通用\",\"level\":\"80\",\"cls\":\"倚天护符,灵果,九宫瓶,紫火花\",\"clc\":\"1,25,72,30\",\"pic\":\"53.jpg\"},\"54\":{\"name\":\"赤霄法冠\",\"pro\":\"通用\",\"level\":\"80\",\"cls\":\"倚天法冠,灵果,九宫瓶,紫火花\",\"clc\":\"1,25,72,30\",\"pic\":\"54.jpg\"},\"55\":{\"name\":\"赤霄战靴\",\"pro\":\"通用\",\"level\":\"80\",\"cls\":\"倚天战靴,灵果,九宫瓶,紫火花\",\"clc\":\"1,25,72,30\",\"pic\":\"55.jpg\"},\"56\":{\"name\":\"天罡弓\",\"pro\":\"飞羽\",\"level\":\"90\",\"cls\":\"赤霄弓,天仙花,仙宝石,云纹金钗\",\"clc\":\"1,15,15,60\",\"pic\":\"tgg.jpg\"},\"57\":{\"name\":\"天罡剑\",\"pro\":\"剑灵\",\"level\":\"90\",\"cls\":\"赤霄剑,天仙花,仙宝石,云纹金钗\",\"clc\":\"1,15,15,60\",\"pic\":\"tgj.jpg\"},\"58\":{\"name\":\"天罡锤\",\"pro\":\"将星\",\"level\":\"90\",\"cls\":\"赤霄斧,天仙花,仙宝石,云纹金钗\",\"clc\":\"1,15,15,60\",\"pic\":\"tgc.jpg\"},\"59\":{\"name\":\"天罡拳套\",\"pro\":\"武圣\",\"level\":\"90\",\"cls\":\"赤霄拳套,天仙花,仙宝石,云纹金钗\",\"clc\":\"1,15,15,60\",\"pic\":\"tgqt.jpg\"},\"60\":{\"name\":\"天罡杖\",\"pro\":\"术士,方士\",\"level\":\"90\",\"cls\":\"赤霄杖,天仙花,仙宝石,云纹金钗\",\"clc\":\"1,15,15,60\",\"pic\":\"tgz.jpg\"},\"61\":{\"name\":\"天罡战魂\",\"pro\":\"飞羽,剑灵,将星,武圣\",\"level\":\"90\",\"cls\":\"赤霄战魂,仙宝石,火焰宝石,玉杯\",\"clc\":\"1,15,30,60\",\"pic\":\"tgzh.jpg\"},\"62\":{\"name\":\"天罡元神\",\"pro\":\"术士,方士\",\"level\":\"90\",\"cls\":\"赤霄战魂,仙宝石,火焰宝石,玉杯\",\"clc\":\"1,15,30,60\",\"pic\":\"tgys.jpg\"},\"63\":{\"name\":\"天罡法袍\",\"pro\":\"通用\",\"level\":\"90\",\"cls\":\"赤霄法袍,天仙花,玉杯,仙宝石\",\"clc\":\"1,15,60,15\",\"pic\":\"tgfp.jpg\"},\"64\":{\"name\":\"天罡护符\",\"pro\":\"通用\",\"level\":\"90\",\"cls\":\"赤霄护符,天仙花,玉杯,仙宝石\",\"clc\":\"1,15,60,15\",\"pic\":\"tghf.jpg\"},\"65\":{\"name\":\"天罡法冠\",\"pro\":\"通用\",\"level\":\"90\",\"cls\":\"赤霄法冠,天仙花,玉杯,仙宝石\",\"clc\":\"1,15,60,15\",\"pic\":\"tgfg.jpg\"},\"66\":{\"name\":\"天罡战靴\",\"pro\":\"通用\",\"level\":\"90\",\"cls\":\"赤霄战靴,天仙花,玉杯,仙宝石\",\"clc\":\"1,15,60,15\",\"pic\":\"tgzx.jpg\"},\"67\":{\"name\":\"乾坤弓\",\"pro\":\"飞羽\",\"level\":\"100\",\"cls\":\"天罡弓,蝎刺,仙人刺,一枝花\",\"clc\":\"1,45,5,10\",\"pic\":\"qkg.jpg\"},\"68\":{\"name\":\"乾坤剑\",\"pro\":\"剑灵\",\"level\":\"100\",\"cls\":\"天罡剑,蝎刺,仙人刺,一枝花\",\"clc\":\"1,45,5,10\",\"pic\":\"qkj.jpg\"},\"69\":{\"name\":\"乾坤宝刀\",\"pro\":\"将星\",\"level\":\"100\",\"cls\":\"天罡锤,蝎刺,仙人刺,一枝花\",\"clc\":\"1,45,5,10\",\"pic\":\"qkbd.jpg\"},\"70\":{\"name\":\"乾坤拳套\",\"pro\":\"武圣\",\"level\":\"100\",\"cls\":\"天罡拳套,蝎刺,仙人刺,一枝花\",\"clc\":\"1,45,5,10\",\"pic\":\"qkqt.jpg\"},\"71\":{\"name\":\"乾坤杖\",\"pro\":\"术士,方士\",\"level\":\"100\",\"cls\":\"天罡杖,蝎刺,仙人刺,一枝花\",\"clc\":\"1,45,5,10\",\"pic\":\"qkz.jpg\"},\"72\":{\"name\":\"乾坤战魂\",\"pro\":\"飞羽,剑灵,将星,武圣\",\"level\":\"100\",\"cls\":\"天罡战魂,龙鳞,仙人刺,一枝花\",\"clc\":\"1,10,5,10\",\"pic\":\"qkzh.jpg\"},\"73\":{\"name\":\"乾坤元神\",\"pro\":\"术士,方士\",\"level\":\"100\",\"cls\":\"天罡元神,龙鳞,仙人刺,一枝花\",\"clc\":\"1,10,5,10\",\"pic\":\"qkys.jpg\"},\"74\":{\"name\":\"乾坤法袍\",\"pro\":\"通用\",\"level\":\"100\",\"cls\":\"天罡法袍,蝎刺,仙人刺,一枝花\",\"clc\":\"1,45,5,10\",\"pic\":\"qkfp.jpg\"},\"75\":{\"name\":\"乾坤护符\",\"pro\":\"通用\",\"level\":\"100\",\"cls\":\"天罡护符,蝎刺,仙人刺,一枝花\",\"clc\":\"1,45,5,10\",\"pic\":\"qkhf.jpg\"},\"76\":{\"name\":\"乾坤法冠\",\"pro\":\"通用\",\"level\":\"100\",\"cls\":\"天罡法冠,蝎刺,仙人刺,一枝花\",\"clc\":\"1,45,5,10\",\"pic\":\"qkfg.jpg\"},\"77\":{\"name\":\"乾坤战靴\",\"pro\":\"通用\",\"level\":\"100\",\"cls\":\"天罡战靴,蝎刺,仙人刺,一枝花\",\"clc\":\"1,45,5,10\",\"pic\":\"qkzx.jpg\"},\"78\":{\"name\":\"腾龙弓\",\"pro\":\"飞羽\",\"level\":\"110\",\"cls\":\"乾坤弓,金蚕丝,五色土,天苍鹰羽\",\"clc\":\"1,75,75,5\",\"pic\":\"tlg.jpg\"},\"79\":{\"name\":\"腾龙剑\",\"pro\":\"剑灵\",\"level\":\"110\",\"cls\":\"乾坤剑,金蚕丝,五色土,天苍鹰羽\",\"clc\":\"1,75,75,5\",\"pic\":\"tlj.jpg\"},\"80\":{\"name\":\"腾龙戟\",\"pro\":\"将星\",\"level\":\"110\",\"cls\":\"乾坤宝刀,金蚕丝,五色土,天苍鹰羽\",\"clc\":\"1,75,75,5\",\"pic\":\"tlji.jpg\"},\"81\":{\"name\":\"腾龙拳套\",\"pro\":\"武圣\",\"level\":\"110\",\"cls\":\"乾坤拳套,金蚕丝,五色土,天苍鹰羽\",\"clc\":\"1,75,75,5\",\"pic\":\"tlqt.jpg\"},\"82\":{\"name\":\"腾龙杖\",\"pro\":\"术士,方士\",\"level\":\"110\",\"cls\":\"乾坤杖,金蚕丝,五色土,天苍鹰羽\",\"clc\":\"1,75,75,5\",\"pic\":\"tlz.jpg\"},\"83\":{\"name\":\"腾龙元神\",\"pro\":\"术士,方士\",\"level\":\"110\",\"cls\":\"乾坤元神,古木,天苍狼爪,天苍鹰羽\",\"clc\":\"1,20,5,5\",\"pic\":\"tlys.jpg\"},\"84\":{\"name\":\"腾龙战魂\",\"pro\":\"飞羽,剑灵,将星,武圣\",\"level\":\"110\",\"cls\":\"乾坤战魂,古木,天苍狼爪,天苍鹰羽\",\"clc\":\"1,20,5,5\",\"pic\":\"tlzh.jpg\"},\"85\":{\"name\":\"腾龙法冠\",\"pro\":\"通用\",\"level\":\"110\",\"cls\":\"乾坤法冠,金蚕丝,天苍狼爪,天苍鹰羽\",\"clc\":\"1,75,5,5\",\"pic\":\"tlfg.jpg\"},\"86\":{\"name\":\"腾龙法袍\",\"pro\":\"通用\",\"level\":\"110\",\"cls\":\"乾坤法袍,金蚕丝,天苍狼爪,天苍鹰羽\",\"clc\":\"1,75,5,5\",\"pic\":\"tlfp.jpg\"},\"87\":{\"name\":\"腾龙战靴\",\"pro\":\"通用\",\"level\":\"110\",\"cls\":\"乾坤战靴,金蚕丝,天苍狼爪,天苍鹰羽\",\"clc\":\"1,75,5,5\",\"pic\":\"tlx.jpg\"},\"88\":{\"name\":\"腾龙护符\",\"pro\":\"通用\",\"level\":\"110\",\"cls\":\"乾坤护符,金蚕丝,天苍狼爪,天苍鹰羽\",\"clc\":\"1,75,5,5\",\"pic\":\"tlfh.jpg\"},\"89\":{\"name\":\"太虚大刀\",\"pro\":\"将星\",\"level\":\"120\",\"cls\":\"腾龙戟,鬼兵面具,饭团,木槿花\",\"clc\":\"1,5,30,30\",\"pic\":\"txd.jpg\"},\"90\":{\"name\":\"太虚弓\",\"pro\":\"飞羽\",\"level\":\"120\",\"cls\":\"腾龙弓,鬼兵面具,饭团,木槿花\",\"clc\":\"1,5,30,30\",\"pic\":\"txg.jpg\"},\"91\":{\"name\":\"太虚剑\",\"pro\":\"剑灵\",\"level\":\"120\",\"cls\":\"腾龙剑,鬼兵面具,饭团,木槿花\",\"clc\":\"1,5,30,30\",\"pic\":\"txj.jpg\"},\"92\":{\"name\":\"太虚杖\",\"pro\":\"术士,方士\",\"level\":\"120\",\"cls\":\"腾龙杖,鬼兵面具,饭团,木槿花\",\"clc\":\"1,5,30,30\",\"pic\":\"txz.jpg\"},\"93\":{\"name\":\"太虚拳套\",\"pro\":\"武圣\",\"level\":\"120\",\"cls\":\"腾龙拳套,鬼兵面具,饭团,木槿花\",\"clc\":\"1,5,30,30\",\"pic\":\"txqt.jpg\"},\"94\":{\"name\":\"太虚法冠\",\"pro\":\"通用\",\"level\":\"120\",\"cls\":\"腾龙法冠,鬼兵面具,黑斗笠,木槿花\",\"clc\":\"1,5,5,30\",\"pic\":\"txfg.jpg\"},\"95\":{\"name\":\"太虚法袍\",\"pro\":\"通用\",\"level\":\"120\",\"cls\":\"腾龙法冠,鬼兵面具,黑斗笠,木槿花\",\"clc\":\"1,5,5,30\",\"pic\":\"txfp.jpg\"},\"96\":{\"name\":\"太虚战靴\",\"pro\":\"通用\",\"level\":\"120\",\"cls\":\"腾龙战靴,鬼兵面具,黑斗笠,木槿花\",\"clc\":\"1,5,5,30\",\"pic\":\"txzx.jpg\"},\"97\":{\"name\":\"太虚护符\",\"pro\":\"通用\",\"level\":\"120\",\"cls\":\"腾龙护符,鬼兵面具,黑斗笠,木槿花\",\"clc\":\"1,5,5,30\",\"pic\":\"txhf.jpg\"},\"98\":{\"name\":\"太虚元神\",\"pro\":\"术士,方士\",\"level\":\"120\",\"cls\":\"腾龙元神,鬼兵面具,黑斗笠,曲玉\",\"clc\":\"1,5,5,30\",\"pic\":\"txys.jpg\"},\"99\":{\"name\":\"太虚战魂\",\"pro\":\"飞羽,剑灵,将星,武圣\",\"level\":\"120\",\"cls\":\"腾龙战魂,鬼兵面具,黑斗笠,曲玉\",\"clc\":\"1,5,5,30\",\"pic\":\"txzh.jpg\"},\"100\":{\"name\":\"魔神拳套\",\"pro\":\"武圣\",\"level\":\"130\",\"cls\":\"太虚拳套,修罗玉,七叶莲,魔石\",\"clc\":\"1,5,30,30\",\"pic\":\"msqt.jpg\"},\"101\":{\"name\":\"魔神剑\",\"pro\":\"剑灵\",\"level\":\"130\",\"cls\":\"太虚剑,修罗玉,七叶莲,魔石\",\"clc\":\"1,5,30,30\",\"pic\":\"msj.jpg\"},\"102\":{\"name\":\"魔神弓\",\"pro\":\"飞羽\",\"level\":\"130\",\"cls\":\"太虚弓,修罗玉,七叶莲,魔石\",\"clc\":\"1,5,30,30\",\"pic\":\"msg.jpg\"},\"103\":{\"name\":\"魔神枪\",\"pro\":\"将星\",\"level\":\"130\",\"cls\":\"太虚大刀,修罗玉,七叶莲,魔石\",\"clc\":\"1,5,30,30\",\"pic\":\"msq.jpg\"},\"104\":{\"name\":\"魔神杖\",\"pro\":\"术士,方士\",\"level\":\"130\",\"cls\":\"太虚杖,修罗玉,七叶莲,魔石\",\"clc\":\"1,5,30,30\",\"pic\":\"msz.jpg\"},\"105\":{\"name\":\"魔神护符\",\"pro\":\"通用\",\"level\":\"130\",\"cls\":\"太虚护符,千年魔花,修罗玉,魔石\",\"clc\":\"1,5,5,30\",\"pic\":\"mshf.jpg\"},\"106\":{\"name\":\"魔神法冠\",\"pro\":\"通用\",\"level\":\"130\",\"cls\":\"太虚法冠,千年魔花,修罗玉,魔石\",\"clc\":\"1,5,5,30\",\"pic\":\"msfg.jpg\"},\"107\":{\"name\":\"魔神法袍\",\"pro\":\"通用\",\"level\":\"130\",\"cls\":\"太虚法袍,千年魔花,修罗玉,魔石\",\"clc\":\"1,5,5,30\",\"pic\":\"msfp.jpg\"},\"108\":{\"name\":\"魔神战靴\",\"pro\":\"通用\",\"level\":\"130\",\"cls\":\"太虚战靴,千年魔花,修罗玉,魔石\",\"clc\":\"1,5,5,30\",\"pic\":\"mszx.jpg\"},\"109\":{\"name\":\"魔神战魂\",\"pro\":\"飞羽,剑灵,将星,武圣\",\"level\":\"130\",\"cls\":\"太虚战魂,千年魔花,修罗玉,神魔号角\",\"clc\":\"1,5,5,30\",\"pic\":\"mszh.jpg\"},\"110\":{\"name\":\"魔神元神\",\"pro\":\"术士,方士\",\"level\":\"130\",\"cls\":\"太虚元神,千年魔花,修罗玉,神魔号角\",\"clc\":\"1,5,5,30\",\"pic\":\"msys.jpg\"},\"111\":{\"name\":\"降魔弓\",\"pro\":\"飞羽\",\"level\":\"10\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"56.jpg\"},\"112\":{\"name\":\"降魔剑\",\"pro\":\"剑灵\",\"level\":\"10\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"57.jpg\"},\"113\":{\"name\":\"降魔锤\",\"pro\":\"将星\",\"level\":\"10\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"58.jpg\"},\"114\":{\"name\":\"降魔拳套\",\"pro\":\"武圣\",\"level\":\"10\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"59.jpg\"},\"115\":{\"name\":\"降魔杖\",\"pro\":\"术士,方士\",\"level\":\"10\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"60.jpg\"},\"116\":{\"name\":\"降魔战魂\",\"pro\":\"飞羽,剑灵,将星,武圣\",\"level\":\"10\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"61.jpg\"},\"117\":{\"name\":\"降魔元神\",\"pro\":\"术士,方士\",\"level\":\"10\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"62.jpg\"},\"118\":{\"name\":\"降魔法袍\",\"pro\":\"通用\",\"level\":\"10\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"63.jpg\"},\"119\":{\"name\":\"降魔护符\",\"pro\":\"通用\",\"level\":\"10\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"64.jpg\"},\"120\":{\"name\":\"降魔法冠\",\"pro\":\"通用\",\"level\":\"10\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"65.jpg\"},\"121\":{\"name\":\"降魔战靴\",\"pro\":\"通用\",\"level\":\"10\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"66.jpg\"},\"122\":{\"name\":\"霸者弓\",\"pro\":\"飞羽\",\"level\":\"40\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"67.jpg\"},\"123\":{\"name\":\"霸者剑\",\"pro\":\"剑灵\",\"level\":\"40\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"68.jpg\"},\"124\":{\"name\":\"霸者锤\",\"pro\":\"将星\",\"level\":\"40\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"69.jpg\"},\"125\":{\"name\":\"霸者拳套\",\"pro\":\"武圣\",\"level\":\"40\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"70.jpg\"},\"126\":{\"name\":\"霸者杖\",\"pro\":\"术士,方士\",\"level\":\"40\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"71.jpg\"},\"127\":{\"name\":\"霸者战魂\",\"pro\":\"飞羽,剑灵,将星,武圣\",\"level\":\"40\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"72.jpg\"},\"128\":{\"name\":\"霸者元神\",\"pro\":\"术士,方士\",\"level\":\"40\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"73.jpg\"},\"129\":{\"name\":\"霸者法袍\",\"pro\":\"通用\",\"level\":\"40\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"74.jpg\"},\"130\":{\"name\":\"霸者护符\",\"pro\":\"通用\",\"level\":\"40\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"75.jpg\"},\"131\":{\"name\":\"霸者法冠\",\"pro\":\"通用\",\"level\":\"40\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"76.jpg\"},\"132\":{\"name\":\"霸者战靴\",\"pro\":\"通用\",\"level\":\"40\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"77.jpg\"},\"133\":{\"name\":\"水晶弓\",\"pro\":\"飞羽\",\"level\":\"60\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"78.jpg\"},\"134\":{\"name\":\"水晶剑\",\"pro\":\"剑灵\",\"level\":\"60\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"79.jpg\"},\"135\":{\"name\":\"水晶枪\",\"pro\":\"将星\",\"level\":\"60\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"80.jpg\"},\"136\":{\"name\":\"水晶拳套\",\"pro\":\"武圣\",\"level\":\"60\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"81.jpg\"},\"137\":{\"name\":\"水晶杖\",\"pro\":\"术士,方士\",\"level\":\"60\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"82.jpg\"},\"138\":{\"name\":\"水晶战魂\",\"pro\":\"飞羽,剑灵,将星,武圣\",\"level\":\"60\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"83.jpg\"},\"139\":{\"name\":\"水晶元神\",\"pro\":\"术士,方士\",\"level\":\"60\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"84.jpg\"},\"140\":{\"name\":\"水晶法袍\",\"pro\":\"通用\",\"level\":\"60\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"85.jpg\"},\"141\":{\"name\":\"水晶护符\",\"pro\":\"通用\",\"level\":\"60\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"86.jpg\"},\"142\":{\"name\":\"水晶法冠\",\"pro\":\"通用\",\"level\":\"60\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"87.jpg\"},\"143\":{\"name\":\"水晶战靴\",\"pro\":\"通用\",\"level\":\"60\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"88.jpg\"},\"144\":{\"name\":\"青虹弓\",\"pro\":\"飞羽\",\"level\":\"70\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"89.jpg\"},\"145\":{\"name\":\"青虹剑\",\"pro\":\"剑灵\",\"level\":\"70\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"90.jpg\"},\"146\":{\"name\":\"青虹大刀\",\"pro\":\"将星\",\"level\":\"70\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"91.jpg\"},\"147\":{\"name\":\"青虹拳套\",\"pro\":\"武圣\",\"level\":\"70\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"92.jpg\"},\"148\":{\"name\":\"青虹杖\",\"pro\":\"术士,方士\",\"level\":\"70\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"93.jpg\"},\"149\":{\"name\":\"青虹战魂\",\"pro\":\"飞羽,剑灵,将星,武圣\",\"level\":\"70\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"94.jpg\"},\"150\":{\"name\":\"青虹元神\",\"pro\":\"术士,方士\",\"level\":\"70\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"95.jpg\"},\"151\":{\"name\":\"青虹法袍\",\"pro\":\"通用\",\"level\":\"70\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"96.jpg\"},\"152\":{\"name\":\"青虹护符\",\"pro\":\"通用\",\"level\":\"70\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"97.jpg\"},\"153\":{\"name\":\"青虹法冠\",\"pro\":\"通用\",\"level\":\"70\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"98.jpg\"},\"154\":{\"name\":\"青虹战靴\",\"pro\":\"通用\",\"level\":\"70\",\"cls\":\"\",\"clc\":\"\",\"pic\":\"99.jpg\"}},sdf345g1df0235235:{\"1\":{\"name\":\"一品武力丹\",\"level\":\"10\",\"cls\":\"落英花\",\"clc\":\"2\",\"pic\":\"1.jpg\"},\"2\":{\"name\":\"一品绝技丹\",\"level\":\"10\",\"cls\":\"落英花\",\"clc\":\"2\",\"pic\":\"2.jpg\"},\"3\":{\"name\":\"一品法术丹\",\"level\":\"10\",\"cls\":\"落英花\",\"clc\":\"2\",\"pic\":\"3.jpg\"},\"4\":{\"name\":\"一品葫芦\",\"level\":\"10\",\"cls\":\"黑水珠,地府风灯\",\"clc\":\"6,6\",\"pic\":\"4.jpg\"},\"5\":{\"name\":\"二品武力丹\",\"level\":\"20\",\"cls\":\"胭脂\",\"clc\":\"9\",\"pic\":\"5.jpg\"},\"6\":{\"name\":\"二品绝技丹\",\"level\":\"20\",\"cls\":\"蓝宝石\",\"clc\":\"9\",\"pic\":\"6.jpg\"},\"7\":{\"name\":\"二品法术丹\",\"level\":\"20\",\"cls\":\"胭脂\",\"clc\":\"9\",\"pic\":\"7.jpg\"},\"8\":{\"name\":\"二品葫芦\",\"level\":\"20\",\"cls\":\"黑水珠,地府风灯\",\"clc\":\"12,12\",\"pic\":\"8.jpg\"},\"9\":{\"name\":\"三品武力丹\",\"level\":\"30\",\"cls\":\"绿珠,象龙角\",\"clc\":\"14,5\",\"pic\":\"9.jpg\"},\"10\":{\"name\":\"三品绝技丹\",\"level\":\"30\",\"cls\":\"念珠,象龙角\",\"clc\":\"14,5\",\"pic\":\"10.jpg\"},\"11\":{\"name\":\"三品法术丹\",\"level\":\"30\",\"cls\":\"念珠,象龙角\",\"clc\":\"14,5\",\"pic\":\"11.jpg\"},\"12\":{\"name\":\"三品葫芦\",\"level\":\"30\",\"cls\":\"黑水珠,地府风灯,天雷珠\",\"clc\":\"36,36,3\",\"pic\":\"12.jpg\"},\"13\":{\"name\":\"四品武力丹\",\"level\":\"40\",\"cls\":\"露凝草,乌舌兰,炎石,血莲花\",\"clc\":\"24,24,24,18\",\"pic\":\"13.jpg\"},\"14\":{\"name\":\"四品绝技丹\",\"level\":\"40\",\"cls\":\"露凝草,玄阴石,炎石,血莲花\",\"clc\":\"24,14,18,18\",\"pic\":\"14.jpg\"},\"15\":{\"name\":\"四品法术丹\",\"level\":\"40\",\"cls\":\"乌舌兰,玄阴石,炎石,血莲花\",\"clc\":\"24,14,18,18\",\"pic\":\"15.jpg\"},\"16\":{\"name\":\"四品葫芦\",\"level\":\"40\",\"cls\":\"魔玉,狼尾草,魔火邪珠,地火胆\",\"clc\":\"29,39,20,29\",\"pic\":\"16.jpg\"},\"17\":{\"name\":\"五品武力丹\",\"level\":\"50\",\"cls\":\"露凝草,炎石,乌舌兰,魔木鼎,冰魄珠\",\"clc\":\"27,27,27,24,36\",\"pic\":\"17.jpg\"},\"18\":{\"name\":\"五品绝技丹\",\"level\":\"50\",\"cls\":\"露凝草,太阴鼎,乌舌兰,魔木鼎,冰魄珠\",\"clc\":\"27,42,27,24,36\",\"pic\":\"18.jpg\"},\"19\":{\"name\":\"五品法术丹\",\"level\":\"50\",\"cls\":\"血莲花,太阴鼎,玄阴石,魔木鼎,冰魄珠\",\"clc\":\"27,42,21,24,36\",\"pic\":\"19.jpg\"},\"20\":{\"name\":\"五品葫芦\",\"level\":\"50\",\"cls\":\"魔火邪珠,陨石,狼尾草,鬼树枝,地火胆\",\"clc\":\"29,45,58,58,44\",\"pic\":\"20.jpg\"},\"21\":{\"name\":\"六品武力丹\",\"level\":\"60\",\"cls\":\"玉珊瑚,黑白珠,天魔果,太阴鼎,地府风灯,天雷珠\",\"clc\":\"42,60,42,42,53,4\",\"pic\":\"21.jpg\"},\"22\":{\"name\":\"六品绝技丹\",\"level\":\"60\",\"cls\":\"玉珊瑚,黑白珠,天魔果,仙树枝,地府风灯,冰魄珠\",\"clc\":\"42,60,42,32,60,42\",\"pic\":\"22.jpg\"},\"23\":{\"name\":\"六品法术丹\",\"level\":\"60\",\"cls\":\"天雷珠,黑白珠,天魔果,太阴鼎,地府风灯,冰魄珠\",\"clc\":\"4,60,42,42,53,42\",\"pic\":\"23.jpg\"}},vwsq12sd7ft8bynoim099:{\"1\":{\"name\":\"狐皮\",\"gets\":\"青丘山1\",\"cls\":\"无\",\"pic\":\"1.jpg\"},\"2\":{\"name\":\"落英花\",\"gets\":\"幽花林1\",\"cls\":\"一品绝技丹,一品法术丹,二品武力丹\",\"pic\":\"2.jpg\"},\"3\":{\"name\":\"蜘蛛牙\",\"gets\":\"幽花林2\",\"cls\":\"无\",\"pic\":\"3.jpg\"},\"4\":{\"name\":\"黑水珠\",\"gets\":\"水下宫殿5\",\"cls\":\"一品丹药葫芦,二品丹药葫芦,三品丹药葫芦,六品法术丹\",\"pic\":\"4.jpg\"},\"5\":{\"name\":\"胭脂\",\"gets\":\"须臾幻境3\",\"cls\":\"二品法术丹,二品武力丹\",\"pic\":\"5.jpg\"},\"6\":{\"name\":\"雷兽角\",\"gets\":\"封神陵2\",\"cls\":\"无\",\"pic\":\"6.jpg\"},\"7\":{\"name\":\"露凝草\",\"gets\":\"灵山5\",\"cls\":\"四品武力丹,四品绝技丹,五品武力丹,五品绝技丹\",\"pic\":\"7.jpg\"},\"8\":{\"name\":\"乌舌兰\",\"gets\":\"灵山1\",\"cls\":\"四品武力丹,四品法术丹,五品武力丹,五品绝技丹\",\"pic\":\"8.jpg\"},\"9\":{\"name\":\"玉珊瑚\",\"gets\":\"水下宫殿1\",\"cls\":\"六品武力丹,六品绝技丹,\",\"pic\":\"9.jpg\"},\"10\":{\"name\":\"天魔果\",\"gets\":\"玄冥界7\",\"cls\":\"六品武力丹,六品绝技丹,六品法术丹\",\"pic\":\"10.jpg\"},\"11\":{\"name\":\"念珠\",\"gets\":\"葬剑谷1\",\"cls\":\"逍遥弓,逍遥剑,逍遥拳套,逍遥杖,逍遥戟,逍遥战魂,逍遥元神,三品绝技丹,三品法术丹\",\"pic\":\"11.jpg\"},\"12\":{\"name\":\"玄天石\",\"gets\":\"封神陵1\",\"cls\":\"逍遥弓,逍遥剑,逍遥拳套,逍遥杖,逍遥戟,逍遥战魂,逍遥元神\",\"pic\":\"12.jpg\"},\"13\":{\"name\":\"象龙角\",\"gets\":\"封神陵4\",\"cls\":\"三品武力丹,三品绝技丹,三品法术丹\",\"pic\":\"13.jpg\"},\"14\":{\"name\":\"不醉石\",\"gets\":\"须臾幻境1\",\"cls\":\"逍遥弓,逍遥剑,逍遥拳套,逍遥杖,逍遥戟,逍遥战魂,逍遥元神,三品法术丹\",\"pic\":\"14.jpg\"},\"15\":{\"name\":\"蓝宝石\",\"gets\":\"须臾幻境5\",\"cls\":\"逍遥法冠,逍遥法袍,逍遥战靴,逍遥护符,二品绝技丹\",\"pic\":\"15.jpg\"},\"16\":{\"name\":\"紫幽羽\",\"gets\":\"封神陵6\",\"cls\":\"逍遥法冠,逍遥法袍,逍遥战靴,逍遥护符\",\"pic\":\"16.jpg\"},\"17\":{\"name\":\"绿珠\",\"gets\":\"须臾幻境7\",\"cls\":\"逍遥法冠,逍遥法袍,逍遥战靴,逍遥护符,三品武力丹\",\"pic\":\"17.jpg\"},\"18\":{\"name\":\"太阴鼎\",\"gets\":\"玄暝界1\",\"cls\":\"五品绝技丹,五品法术丹,六品武力丹,六品法术丹\",\"pic\":\"18.jpg\"},\"19\":{\"name\":\"魔木鼎\",\"gets\":\"大荒漠城1\",\"cls\":\"朱雀拳套,朱雀剑,朱雀弓,朱雀枪,朱雀杖,朱雀法冠,朱雀法袍,朱雀战靴,朱雀护符,朱雀战魂,朱雀元神,五品武力丹,五品绝技丹,五品法术丹\",\"pic\":\"19.jpg\"},\"20\":{\"name\":\"炎石\",\"gets\":\"烈焰洞1\",\"cls\":\"朱雀拳套,朱雀剑,朱雀弓,朱雀枪,朱雀杖,朱雀战魂,朱雀元神,四品武力丹,四品绝技丹,四品法术丹,五品武力丹\",\"pic\":\"20.jpg\"},\"21\":{\"name\":\"冰魄珠\",\"gets\":\"玉都峰4\",\"cls\":\"五品武力丹,五品绝技丹,五品法术丹,六品绝技丹\",\"pic\":\"21.jpg\"},\"22\":{\"name\":\"玄阴石\",\"gets\":\"炼妖塔8\",\"cls\":\"朱雀拳套,朱雀剑,朱雀弓,朱雀枪,朱雀杖,朱雀战魂,朱雀元神,四品绝技丹,四品法术丹,五品法术丹\",\"pic\":\"22.jpg\"},\"23\":{\"name\":\"凤凰羽\",\"gets\":\"灵山8\",\"cls\":\"朱雀法冠,朱雀法袍,朱雀战靴,朱雀护符\",\"pic\":\"23.jpg\"},\"24\":{\"name\":\"血莲花\",\"gets\":\"炼妖塔4\",\"cls\":\"朱雀法冠,朱雀法袍,朱雀战靴,朱雀护符,四品武力丹,四品绝技丹,四品法术丹,五品法术丹\",\"pic\":\"24.jpg\"},\"25\":{\"name\":\"天雷珠\",\"gets\":\"虚天殿6\",\"cls\":\"玄奇拳套,玄奇剑,玄奇弓,玄奇棍,玄奇杖,玄奇战魂,玄奇元神,玄奇法冠,玄奇法袍,玄奇战靴,玄奇护符,六品武力丹,六品法术丹,三品丹药葫芦\",\"pic\":\"25.jpg\"},\"26\":{\"name\":\"至尊鼎\",\"gets\":\"虚天殿8\",\"cls\":\"玄奇拳套,玄奇剑,玄奇弓,玄奇棍,玄奇杖,玄奇战魂,玄奇元神,玄奇法冠,玄奇法袍,玄奇战靴,玄奇护符\",\"pic\":\"26.jpg\"},\"27\":{\"name\":\"魔火邪珠\",\"gets\":\"蜀山密道4\",\"cls\":\"四品丹药葫芦,五品丹药葫芦\",\"pic\":\"27.jpg\"},\"28\":{\"name\":\"魔玉\",\"gets\":\"蜀山秘道2\",\"cls\":\"玄奇拳套,玄奇剑,玄奇弓,玄奇棍,玄奇杖,玄奇战魂,玄奇元神,四品丹药葫芦\",\"pic\":\"28.jpg\"},\"29\":{\"name\":\"陨石\",\"gets\":\"扶桑神树1\",\"cls\":\"倚天拳套,倚天剑,倚天弓,倚天大刀,倚天杖,倚天法冠,倚天法袍,倚天战靴,倚天护符,五品丹药葫芦\",\"pic\":\"29.jpg\"},\"30\":{\"name\":\"仙树枝\",\"gets\":\"虚天殿2\",\"cls\":\"六品绝技丹\",\"pic\":\"30.jpg\"},\"31\":{\"name\":\"地府风灯\",\"gets\":\"幽冥地府4\",\"cls\":\"玄奇法冠,玄奇法袍,玄奇战靴,玄奇护符,一品丹药葫芦,二品丹药葫芦,三品丹药葫芦,六品武力丹,六品绝技丹,六品法术丹\",\"pic\":\"31.jpg\"},\"32\":{\"name\":\"黑白珠\",\"gets\":\"水下宫殿7\",\"cls\":\"六品武力丹,六品绝技丹,六品法术丹\",\"pic\":\"32.jpg\"},\"33\":{\"name\":\"地火胆\",\"gets\":\"大裂谷1\",\"cls\":\"四品丹药葫芦,五品丹药葫芦\",\"pic\":\"33.jpg\"},\"34\":{\"name\":\"狼尾草\",\"gets\":\"大裂谷4\",\"cls\":\"倚天拳套,倚天剑,倚天弓,倚天大刀,倚天杖,倚天战魂,倚天元神,倚天法冠,倚天法袍,倚天战靴,倚天护符,四品丹药葫芦,五品丹药葫芦\",\"pic\":\"34.jpg\"},\"35\":{\"name\":\"鬼树枝\",\"gets\":\"扶桑神树7\",\"cls\":\"倚天战魂,倚天元神,五品丹药葫芦\",\"pic\":\"35.jpg\"},\"36\":{\"name\":\"天心花\",\"gets\":\"蜀山秘道7\",\"cls\":\"倚天拳套,倚天剑,倚天弓,倚天大刀,倚天杖,倚天战魂,倚天元神,倚天法冠,倚天法袍,倚天战靴,倚天护符\",\"pic\":\"36.jpg\"},\"37\":{\"name\":\"灵果\",\"gets\":\"冰晶河5\",\"cls\":\"赤霄拳套,赤霄剑,赤霄弓,赤霄斧,赤霄杖,赤霄法冠,赤霄法袍,赤霄战靴,赤霄护符\",\"pic\":\"37.jpg\"},\"38\":{\"name\":\"辟邪玉\",\"gets\":\"火焰山\",\"cls\":\"无\",\"pic\":\"38.jpg\"},\"39\":{\"name\":\"麒麟角\",\"gets\":\"火焰山8\",\"cls\":\"赤霄拳套,赤霄剑,赤霄弓,赤霄斧,赤霄杖\",\"pic\":\"39.jpg\"},\"40\":{\"name\":\"紫火花\",\"gets\":\"火焰山7\",\"cls\":\"赤霄拳套,赤霄剑,赤霄弓,赤霄斧,赤霄杖,赤霄战魂,赤霄元神,赤霄法冠,赤霄法袍,赤霄战靴,赤霄护符\",\"pic\":\"40.jpg\"},\"41\":{\"name\":\"九宫瓶\",\"gets\":\"玄静寺1\",\"cls\":\"赤霄战魂,赤霄元神,赤霄法冠,赤霄法袍,赤霄战靴,赤霄护符\",\"pic\":\"41.jpg\"},\"42\":{\"name\":\"狼骨\",\"gets\":\"冰晶河\",\"cls\":\"无\",\"pic\":\"42.jpg\"},\"43\":{\"name\":\"黑流玉\",\"gets\":\"玄静寺8\",\"cls\":\"赤霄战魂,赤霄元神\",\"pic\":\"43.jpg\"},\"44\":{\"name\":\"仙宝石\",\"gets\":\"天镜2\",\"cls\":\"天罡弓,天罡剑,天罡拳套,天罡杖,天罡锤,天罡护符,天罡法袍,天罡法冠,天罡战靴,天罡战魂,天罡元神\",\"pic\":\"44.jpg\"},\"45\":{\"name\":\"火焰宝石\",\"gets\":\"东瀛2\",\"cls\":\"天罡元神,天罡战魂\",\"pic\":\"45.jpg\"},\"46\":{\"name\":\"玉杯\",\"gets\":\"天镜2\",\"cls\":\"天罡元神,天罡战魂,天罡护符,天罡法袍,天罡法冠,天罡战靴\",\"pic\":\"46.jpg\"},\"47\":{\"name\":\"不灭灯\",\"gets\":\"天镜\",\"cls\":\"无\",\"pic\":\"47.jpg\"},\"48\":{\"name\":\"天仙花\",\"gets\":\"跌水崖7\",\"cls\":\"天罡弓,天罡剑,天罡拳套,天罡杖,天罡锤,天罡护符,天罡法袍,天罡法冠,天罡战靴\",\"pic\":\"48.jpg\"},\"49\":{\"name\":\"绝色玉\",\"gets\":\"东瀛\",\"cls\":\"无\",\"pic\":\"49.jpg\"},\"50\":{\"name\":\"云纹金钗\",\"gets\":\"跌水崖2\",\"cls\":\"天罡弓,天罡剑,天罡拳套,天罡杖,天罡锤\",\"pic\":\"50.jpg\"},\"51\":{\"name\":\"蝎刺\",\"gets\":\"水陆道场5\",\"cls\":\"乾坤弓,乾坤剑,乾坤拳套,乾坤杖,乾坤宝刀,乾坤护符,乾坤法袍,乾坤法冠,乾坤战靴\",\"pic\":\"51.jpg\"},\"52\":{\"name\":\"仙人刺\",\"gets\":\"奈何桥3\",\"cls\":\"乾坤元神,乾坤战魂,乾坤弓,乾坤剑,乾坤拳套,乾坤宝刀,乾坤杖,乾坤护符,乾坤法袍,乾坤法冠,乾坤战靴\",\"pic\":\"52.jpg\"},\"53\":{\"name\":\"血凝爪\",\"gets\":\"秦始皇陵\",\"cls\":\"无\",\"pic\":\"53.jpg\"},\"54\":{\"name\":\"龙鳞\",\"gets\":\"秦始皇陵6\",\"cls\":\"乾坤元神,乾坤战魂\",\"pic\":\"54.jpg\"},\"55\":{\"name\":\"一枝花\",\"gets\":\"奈何桥9\",\"cls\":\"乾坤元神,乾坤战魂,乾坤弓,乾坤剑,乾坤拳套,乾坤杖,乾坤宝刀,乾坤护符,乾坤法袍,乾坤法冠,乾坤战靴\",\"pic\":\"55.jpg\"},\"56\":{\"name\":\"霸王鼎\",\"gets\":\"秦始皇陵\",\"cls\":\"无\",\"pic\":\"56.jpg\"},\"57\":{\"name\":\"妖果\",\"gets\":\"长安城\",\"cls\":\"无\",\"pic\":\"57.jpg\"},\"58\":{\"name\":\"金蚕丝\",\"gets\":\"太一城蛮荒丛林1\",\"cls\":\"腾龙拳套,腾龙戟,腾龙剑,腾龙弓,腾龙杖,腾龙护符,腾龙法袍,腾龙法冠,腾龙战靴\",\"pic\":\"jcs.jpg\"},\"59\":{\"name\":\"五色土\",\"gets\":\"太一城蛮荒丛林4\",\"cls\":\"腾龙拳套,腾龙戟,腾龙剑,腾龙弓,腾龙杖\",\"pic\":\"wst.jpg\"},\"60\":{\"name\":\"天苍鹰羽\",\"gets\":\"太一城天苍草原3\",\"cls\":\"腾龙拳套,腾龙戟,腾龙剑,腾龙弓,腾龙杖,腾龙护符,腾龙法袍,腾龙法冠,腾龙战靴,腾龙战魂,腾龙元神\",\"pic\":\"tcyy.jpg\"},\"61\":{\"name\":\"天苍狼爪\",\"gets\":\"太一城天苍草原6\",\"cls\":\"腾龙护符,腾龙法袍,腾龙法冠,腾龙战靴,腾龙战魂,腾龙元神\",\"pic\":\"tcly.jpg\"},\"62\":{\"name\":\"古木\",\"gets\":\"太一城古城废墟1\",\"cls\":\"腾龙战魂,腾龙元神\",\"pic\":\"gm.jpg\"},\"63\":{\"name\":\"鬼兵面具\",\"gets\":\"高丽城黑暗幕府3层\",\"cls\":\"太虚大刀,太虚弓,太虚剑,太虚杖,太虚拳套,太虚法冠,太虚法袍,太虚战靴,太虚护符,太虚战魂,太虚元神\",\"pic\":\"gcmj.jpg\"},\"64\":{\"name\":\"饭团\",\"gets\":\"高丽王宫4\",\"cls\":\"太虚大刀,太虚弓,太虚剑,太虚杖,太虚拳套\",\"pic\":\"ft.jpg\"},\"65\":{\"name\":\"木槿花\",\"gets\":\"高丽王宫1层\",\"cls\":\"太虚大刀,太虚弓,太虚剑,太虚杖,太虚拳套,太虚法冠,太虚法袍,太虚战靴,太虚护符\",\"pic\":\"mjh.jpg\"},\"66\":{\"name\":\"黑斗笠\",\"gets\":\"高丽城黑暗幕府6层\",\"cls\":\"太虚大刀,太虚弓,太虚剑,太虚杖,太虚拳套,太虚法冠,太虚法袍,太虚战靴,太虚护符\",\"pic\":\"hdl.jpg\"},\"67\":{\"name\":\"曲玉\",\"gets\":\"黑暗皇城1层\",\"cls\":\"太虚元神,太虚战魂\",\"pic\":\"qy.jpg\"},\"68\":{\"name\":\"修罗玉\",\"gets\":\"魔都修罗塔3层\",\"cls\":\"魔神拳套,魔神剑,魔神弓,魔神枪,魔神杖,魔神护符,魔神法冠,魔神法袍,魔神战靴,魔神战魂,魔神元神\",\"pic\":\"xly.jpg\"},\"69\":{\"name\":\"七叶莲\",\"gets\":\"暂无\",\"cls\":\"魔神拳套,魔神剑,魔神弓,魔神枪,魔神杖\",\"pic\":\"qyl.jpg\"},\"70\":{\"name\":\"魔石\",\"gets\":\"魔都暗巷1层\",\"cls\":\"魔神拳套,魔神剑,魔神弓,魔神枪,魔神杖,魔神护符,魔神法冠,魔神法袍,魔神战靴\",\"pic\":\"ms.jpg\"},\"71\":{\"name\":\"千年魔花\",\"gets\":\" 魔都修罗塔6层\",\"cls\":\"魔神护符,魔神法冠,魔神法袍,魔神战靴,魔神战魂、魔神元神\",\"pic\":\"qnmh.jpg\"},\"72\":{\"name\":\"神魔号角\",\"gets\":\"魔都神魔战场1层\",\"cls\":\"魔神战魂,魔神元神\",\"pic\":\"smhj.jpg\"}},hotkw:{\"0\":\"魔神\",\"1\":\"太虚\",\"2\":\"念珠\",\"3\":\"不醉石\",\"4\":\"蓝宝石\",\"5\":\"曲玉\",\"6\":\"鬼才面具\",\"7\":\"逍遥\"}}";

    private static final String EXCEL_SPACE = "\t";

    // TODO 导出来会乱序的。。暂时不改了~凑合先写好逻辑
    public static void main(String[] args) {
//        Temp temp = JSON.parseObject(HTML, Temp.class);
        JSONObject obj = JSON.parseObject(HTML);
        // 输出材料
//        JSONObject gvbhjnngfdew34drftvb = obj.getJSONObject("vwsq12sd7ft8bynoim099");
//        for (Map.Entry<String, Object> entry : gvbhjnngfdew34drftvb.entrySet()) {
//            JSONObject item = (JSONObject) entry.getValue();
//            System.out.println(item.get("name"));
//        }
        // 三品以内都绿色 四品开始蓝色 六品紫色
//        int j = 1501; // 1501 - 2000
//        ItemTemplate.load();
//        JSONObject gvbhjnngfdew34drftvb = obj.getJSONObject("sdf345g1df0235235");
//        for (Map.Entry<String, Object> entry : gvbhjnngfdew34drftvb.entrySet()) {
//            JSONObject item = (JSONObject) entry.getValue();
//            String name = item.getString("name");
//            String[] mats = item.getString("cls").split(",");
//            int[] matCounts = StringUtils.splitInt(item.getString("clc"), ",");
//            boolean error = false;
//            for (int i = 0; i < mats.length; i++) { // 排除掉缺少的模版
//                ItemTemplate template = ItemTemplate.getItemTemplate(mats[i]);
//                if (template == null) {
//                    error = true;
//                    break;
//                }
//            }
//            if (error) {
//                continue;
//            }
//            System.out.print(j);
//
//            // end [base]
//            System.out.print(EXCEL_SPACE);
//            System.out.print(name);
//            System.out.print(EXCEL_SPACE);
//            ItemDef.Quality quality = ItemDef.Quality.GREEN;
//            if (name.startsWith("六")) {
//                quality = ItemDef.Quality.PURPLE;
//            } else if (name.startsWith("四") || name.startsWith("五")) {
//                quality = ItemDef.Quality.BLUE;
//            }
//            System.out.print(quality.getIndex());
//            System.out.print(EXCEL_SPACE);
//            System.out.print(ItemDef.IdentityType.CONSUMABLE.getIndex());
//            System.out.print(EXCEL_SPACE);
//            System.out.print(ItemDef.Type.MEDICINE_CONSUMABLE.getIndex());
//            System.out.print(EXCEL_SPACE);
//            System.out.print("");
//            System.out.print(EXCEL_SPACE);
//            System.out.println();
//            j++;
//        }

        // 输出装备合成卷轴 // xxx 制作卷 都是紫色
        int j = 1001; // 1001 - 1200
        ItemTemplate.load();
        JSONObject gvbhjnngfdew34drftvb = obj.getJSONObject("gvbhjnngfdew34drftvb");
        for (Map.Entry<String, Object> entry : gvbhjnngfdew34drftvb.entrySet()) {
            JSONObject item = (JSONObject) entry.getValue();
            String name = item.getString("name") + "制作卷";
            String getName = item.getString("name");
            String[] mats = item.getString("cls").split(",");
            int[] matCounts = StringUtils.splitInt(item.getString("clc"), ",");
            boolean error = false;
            for (String mat : mats) { // 排除掉缺少的模版
                ItemTemplate template = ItemTemplate.getItemTemplate(mat);
                if (template == null) {
                    error = true;
                    break;
                }
            }
            if (error || ItemTemplate.getItemTemplate(getName) == null) {
                continue;
            }
            System.out.print(j);

//            // end [base]
            System.out.print(EXCEL_SPACE);
            System.out.print(name);
            System.out.print(EXCEL_SPACE);
            System.out.print(ItemDef.Quality.PURPLE.getIndex());
            System.out.print(EXCEL_SPACE);
            System.out.print(ItemDef.IdentityType.REEL.getIndex());
            System.out.print(EXCEL_SPACE);
            System.out.print(ItemDef.Type.EQUIP_REEL.getIndex());
            // begin [base]
            System.out.print(EXCEL_SPACE);
            System.out.print(ItemTemplate.getItemTemplate(getName).getId());
            System.out.print(EXCEL_SPACE);
            System.out.print(1);
            for (int i = 0; i < mats.length; i++) {
                ItemTemplate template = ItemTemplate.getItemTemplate(mats[i]);
                System.out.print(EXCEL_SPACE);
                System.out.print(template.getId() + ";" + matCounts[i]);
            }
            System.out.println();
            j++;
        }

//        // 三品以内都绿色 四品开始蓝色 六品紫色
//        int j = 1401; // 1401 - 1300
//        ItemTemplate.load();
//        JSONObject gvbhjnngfdew34drftvb = obj.getJSONObject("sdf345g1df0235235");
//        for (Map.Entry<String, Object> entry : gvbhjnngfdew34drftvb.entrySet()) {
//            JSONObject item = (JSONObject) entry.getValue();
//            String name = item.getString("name") + "制作卷";
//            String getName = item.getString("name");
//            int getCount = 1;
//            String[] mats = item.getString("cls").split(",");
//            int[] matCounts = StringUtils.splitInt(item.getString("clc"), ",");
//            boolean error = false;
//            for (int i = 0; i < mats.length; i++) { // 排除掉缺少的模版
//                ItemTemplate template = ItemTemplate.getItemTemplate(mats[i]);
//                if (template == null) {
//                    error = true;
//                    break;
//                }
//            }
//            if (error) {
//                continue;
//            }
//            ItemDef.Quality quality = null;
//            if (name.startsWith("六")) {
//                if (name.contains("葫芦")) getCount = 1;
//                quality = ItemDef.Quality.PURPLE;
//            } else if (name.startsWith("五")) {
//                if (name.contains("葫芦")) getCount = 2;
//                quality = ItemDef.Quality.BLUE;
//            } else if (name.startsWith("四")) {
//                if (name.contains("葫芦")) getCount = 3;
//                quality = ItemDef.Quality.BLUE;
//            }  else if (name.startsWith("三")) {
//                if (name.contains("葫芦")) getCount = 4;
//                quality = ItemDef.Quality.GREEN;
//            }  else if (name.startsWith("二")) {
//                if (name.contains("葫芦")) getCount = 5;
//                quality = ItemDef.Quality.GREEN;
//            }  else if (name.startsWith("一")) {
//                if (name.contains("葫芦")) getCount = 3;
//                quality = ItemDef.Quality.GREEN;
//            }
//
//            System.out.print(j);
//
////            // end [base]
////            System.out.print(EXCEL_SPACE);
////            System.out.print(name);
////            System.out.print(EXCEL_SPACE);
////            System.out.print(quality.getIndex());
////            System.out.print(EXCEL_SPACE);
////            System.out.print(ItemDef.IdentityType.REEL.getIndex());
////            System.out.print(EXCEL_SPACE);
////            System.out.print(ItemDef.Type.MEDICINE_REEL.getIndex());
////            // begin [base]
//            System.out.print(EXCEL_SPACE);
//            System.out.print(ItemTemplate.getItemTemplate(getName).getId());
//            System.out.print(EXCEL_SPACE);
//            System.out.print(getCount);
//            for (int i = 0; i < mats.length; i++) {
//                ItemTemplate template = ItemTemplate.getItemTemplate(mats[i]);
//                System.out.print(EXCEL_SPACE);
//                System.out.print(template.getId() + ";" + matCounts[i]);
//            }
//            System.out.println();
//            j++;
//        }
    }

}
