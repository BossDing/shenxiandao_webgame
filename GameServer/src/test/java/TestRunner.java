import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: yunai
 * Date: 13-5-17
 * Time: 上午9:00
 */
public class TestRunner {

    private static class R1 implements Runnable {

        private int max;

        private R1(int max) {
            this.max = max;
        }

        @Override
        public void run() {
//            long sum = 0;
//            for (int i = 0; i < max; i++) {
//                sum += i;
//            }
//            System.out.println("sum: " + sum);
        }
    }

    public static void main(String[] args) {
        int k = 1000;
        long ss = 0;
        long _ss = 0;

        for (int j = 0; j < k; j++) {

            int nCount = 20000;
            int max = 1;
            int limit = nCount / max;
    //        System.out.println("时间：" + System.nanoTime());
            long s1 = System.nanoTime();
            long _s1 = System.currentTimeMillis();
            ExecutorService executor = Executors.newFixedThreadPool(10);

            for (int i = 0; i < limit; i++) {
                executor.submit(new R1(max));
            }

            executor.shutdown();

            long s2 = System.nanoTime();
            long _s2 = System.currentTimeMillis();
            ss += (s2 - s1);
            _ss += (_s2 - _s1);
            System.out.println(s2 - s1);
            System.out.println(_s2 - _s1);
        }
        System.out.println("时间：" + ss / k);
        System.out.println("时间：" + _ss / k);
    }

}
