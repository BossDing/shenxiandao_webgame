package org.yunai.swjg.server.core.util;

/**
 * 角色属性Utils
 * User: yunai
 * Date: 13-5-3
 * Time: 下午10:06
 */
public class RolePropertyUtils {

    private RolePropertyUtils() {
    }

    /**
     * 丹药增加属性公差
     */
    public static final int MEDICINE_PROP_D = -5;

    /**
     * 计算medicineLevel丹药第index个属性加成<br />
     * 公式为：(2 + 丹药属性加成等级 - 该等级数量) * 5
     *
     * @param medicineLevel 丹药属性加成等级
     * @param index         该等级数量
     * @return 属性加成
     */
    public static int genMedicinePropAdd(int medicineLevel, int index) {
        return (2 + medicineLevel - index) * -MEDICINE_PROP_D;
    }

    /**
     * 计算medicineLevel丹药的总属性加成
     *
     * @param medicineLevel 丹药属性加成等级
     * @param count 丹药数量
     * @return 总属性加成
     */
    public static int genMedicinePropAddTotal(int medicineLevel, int count) {
        return (genMedicinePropAdd(medicineLevel, 1) + genMedicinePropAdd(medicineLevel, count)) * count / 2;
    }
}
