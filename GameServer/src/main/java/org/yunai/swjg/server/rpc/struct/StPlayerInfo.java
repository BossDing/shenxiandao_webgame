package org.yunai.swjg.server.rpc.struct;

import org.yunai.yfserver.message.*;

/**
 * 玩家信息结构体
 */
public class StPlayerInfo implements IStruct {
    /**
     * 玩家编号
     */
    private Integer id;
    /**
     * 等级
     */
    private Short level;
    /**
     * 玩家昵称
     */
    private String nickname;
    /**
     * 场景坐标X
     */
    private Short sceneX;
    /**
     * 场景坐标Y
     */
    private Short sceneY;
    /**
     * 目标场景坐标Y
     */
    private Short toSceneX;
    /**
     * 目标场景坐标Y
     */
    private Short toSceneY;

    public StPlayerInfo() {
    }

    public StPlayerInfo(Integer id, Short level, String nickname, Short sceneX, Short sceneY, Short toSceneX, Short toSceneY) {
        this.id = id;
        this.level = level;
        this.nickname = nickname;
        this.sceneX = sceneX;
        this.sceneY = sceneY;
        this.toSceneX = toSceneX;
        this.toSceneY = toSceneY;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public Short getLevel() {
		return level;
	}

	public void setLevel(Short level) {
		this.level = level;
	}
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public Short getSceneX() {
		return sceneX;
	}

	public void setSceneX(Short sceneX) {
		this.sceneX = sceneX;
	}
	public Short getSceneY() {
		return sceneY;
	}

	public void setSceneY(Short sceneY) {
		this.sceneY = sceneY;
	}
	public Short getToSceneX() {
		return toSceneX;
	}

	public void setToSceneX(Short toSceneX) {
		this.toSceneX = toSceneX;
	}
	public Short getToSceneY() {
		return toSceneY;
	}

	public void setToSceneY(Short toSceneY) {
		this.toSceneY = toSceneY;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            StPlayerInfo struct = new StPlayerInfo();
            struct.setId(byteArray.getInt());
            struct.setLevel(byteArray.getShort());
            struct.setNickname(getString(byteArray));
            struct.setSceneX(byteArray.getShort());
            struct.setSceneY(byteArray.getShort());
            struct.setToSceneX(byteArray.getShort());
            struct.setToSceneY(byteArray.getShort());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            StPlayerInfo struct = (StPlayerInfo) message;
            ByteArray byteArray = ByteArray.createNull(14);
            byte[] nicknameBytes = convertString(byteArray, struct.getNickname());
            byteArray.create();
            byteArray.putInt(struct.getId());
            byteArray.putShort(struct.getLevel());
            putString(byteArray, nicknameBytes);
            byteArray.putShort(struct.getSceneX());
            byteArray.putShort(struct.getSceneY());
            byteArray.putShort(struct.getToSceneX());
            byteArray.putShort(struct.getToSceneY());
            return byteArray;
        }
    }
}