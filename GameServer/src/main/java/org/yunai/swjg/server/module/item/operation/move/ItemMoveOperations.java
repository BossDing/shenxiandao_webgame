package org.yunai.swjg.server.module.item.operation.move;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.module.item.container.CommonBag;
import org.yunai.swjg.server.module.item.vo.Item;
import org.yunai.swjg.server.module.player.vo.Player;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 道具移动操作集合
 * User: yunai
 * Date: 13-6-6
 * Time: 下午8:08
 */
@Component
public class ItemMoveOperations {

    @Resource
    private MoveShoulderBag2EquipmentBagOperation moveEquipment2EquipmentBagOperation;
    @Resource
    private MoveEquipmentBag2ShoulderBagOperation moveEquipment2ShoulderBagOperation;
    @Resource
    private MoveShoulderBag2ShoulderBagOperation moveShoulderBag2ShoulderBagOperation;

    /**
     * 移动操作集合
     */
    private final List<ItemMoveOperation> operations;

    public ItemMoveOperations() {
        this.operations = new ArrayList<>();
    }

    @PostConstruct
    public void init() {
        operations.add(moveEquipment2EquipmentBagOperation);
        operations.add(moveEquipment2ShoulderBagOperation);
        operations.add(moveShoulderBag2ShoulderBagOperation);
    }

    /**
     * 获得对应的移动操作
     *
     * @param player  玩家信息
     * @param item    道具
     * @param toBag   目标背包
     * @param toIndex 目标背包位置
     * @return 移动操作
     */
    public ItemMoveOperation getSuitableOperation(Player player, Item item, CommonBag toBag, int toIndex) {
        for (ItemMoveOperation operation : operations) {
            if (operation.isSuitable(player, item, toBag, toIndex)) {
                return operation;
            }
        }
        return null;
    }
}