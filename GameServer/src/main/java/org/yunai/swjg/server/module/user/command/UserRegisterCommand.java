package org.yunai.swjg.server.module.user.command;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.user.operation.UserRegisterIoOperation;
import org.yunai.swjg.server.rpc.message.C_S.C_S_RegisterReq;
import org.yunai.yfserver.async.IIoOperationService;

import javax.annotation.Resource;

/**
 * 用户注册命令
 * User: yunai
 * Date: 13-3-30
 * Time: 下午3:11
 */
@Component
public class UserRegisterCommand extends GameMessageCommand<C_S_RegisterReq> {

    @Resource
    private IIoOperationService ioOperationService;

    @Override
    public void execute(Online online, C_S_RegisterReq msg) {
        ioOperationService.asyncExecute(new UserRegisterIoOperation(online.getSession(), msg.getUserName(), msg.getPassword()));
    }
}
