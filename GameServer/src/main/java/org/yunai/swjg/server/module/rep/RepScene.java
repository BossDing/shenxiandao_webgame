package org.yunai.swjg.server.module.rep;

import org.slf4j.Logger;
import org.yunai.swjg.server.core.annotation.SceneThread;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.currency.Currency;
import org.yunai.swjg.server.module.currency.CurrencyService;
import org.yunai.swjg.server.module.monster.vo.VisibleMonster;
import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.swjg.server.module.rep.template.RepTemplate;
import org.yunai.swjg.server.module.scene.core.AbstractScene;
import org.yunai.swjg.server.rpc.message.S_C.*;
import org.yunai.swjg.server.rpc.struct.StVisibleMonsterInfo;
import org.yunai.yfserver.common.LoggerFactory;
import org.yunai.yfserver.spring.BeanManager;
import org.yunai.yfserver.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 副本场景
 * User: yunai
 * Date: 13-5-12
 * Time: 下午10:32
 */
public class RepScene extends AbstractScene {

    private static final Logger LOGGER_REP = LoggerFactory.getLogger(LoggerFactory.Logger.rep, RepScene.class);

    private static CurrencyService currencyService;
    private static RepSceneService repSceneService;
    static {
        currencyService = BeanManager.getBean(CurrencyService.class);
        repSceneService = BeanManager.getBean(RepSceneService.class);
    }

    /**
     * 副本场景最大人数
     */
    private static final int PLAYER_MAX_NUM = 1;

    /**
     * 副本信息模版
     */
    private final RepTemplate repTemplate;
    /**
     * 副本怪物管理
     */
    private final RepMonsterManager monsterManager;

    public RepScene(RepTemplate repTemplate) {
        super(repTemplate.getSceneTemplate());
        this.repTemplate = repTemplate;
        this.monsterManager = new RepMonsterManager(this);
    }

    public void init() {
        monsterManager.init();
    }

    /**
     * 玩家进入场景
     *
     * @param online 在线信息
     */
    @SceneThread
    public void onPlayerEnter(Online online) {
        // 发送给自己进入副本成功
        Player player = online.getPlayer();
        online.write(new S_C_RepEnterSuccessResp());
        // 场景中的一些广播
        super.onPlayerEnter(online);
        // 发送怪物信息
        List<VisibleMonster> monsters  = monsterManager.getAliveMonsters();
        List<StVisibleMonsterInfo> stMonsters = new ArrayList<>(monsters.size());
        for (VisibleMonster monster : monsters) {
            stMonsters.add(monster.genStVisibleMonsterInfo());
        }
        online.write(new S_C_MonsterListResp(stMonsters));
    }

    @Override
    public boolean isFull() {
        return super.sceneOnlineManager.size() >= PLAYER_MAX_NUM;
    }

    public RepTemplate getRepTemplate() {
        return repTemplate;
    }

//    public void onMonsterDead(VisibleMonster monster) {
//        // 怪物移除消息
////        Online battleOnline = monster.getOnline();
////        battleOnline.write(new S_C_MonsterDelResp(monster.getId()));
//        S_C_MonsterDelResp monsterDelResp = new S_C_MonsterDelResp(monster.getId());
//        sceneOnlineManager.sendGameMessage(monsterDelResp, (Set<Integer>) Collections.emptySet());
//        // 副本是否结束
//
//    }

    /**
     * 与副本怪物战斗后的逻辑
     *
     * @param battleOnline 战斗的玩家
     * @param monster 怪物
     * @param attWin 战斗胜负结果
     */
    public void afterBattleMonster(Online battleOnline, VisibleMonster monster, boolean attWin) {
        if (!attWin) {
            repSceneService.onDefaultRelive(battleOnline);
            return;
        }
        // 广播怪物死亡
        monster.setAlive(false);
        S_C_MonsterDelResp monsterDelResp = new S_C_MonsterDelResp(monster.getId());
        sceneOnlineManager.sendGameMessage(monsterDelResp, CollectionUtils.emptySet(Integer.class));
        // 判断副本是否结束，若结束，则发奖励
        if (monsterManager.aliveMonsterCount() == 0) {
            // 广播副本结束
            S_C_RepCompleteResp repCompleteResp = new S_C_RepCompleteResp();
            sceneOnlineManager.sendGameMessage(repCompleteResp, CollectionUtils.emptySet(Integer.class));
            // 发奖励
            Set<Integer> playerIds = sceneOnlineManager.getPlayerIds();
            for (Integer playerId : playerIds) {
                Online online = sceneOnlineManager.getOnline(playerId);
                if (online == null) {
                    continue;
                }
                Player player = online.getPlayer();
                // 奖励经验
                player.addExp(repTemplate.getBonusExp());
                // 奖励铜钱
                Integer bonusCoin = 100; // TODO 暂时测试
                if (currencyService.canGive(player, Currency.COIN, bonusCoin, false)) {
                    currencyService.give(player, Currency.COIN, bonusCoin);
                }
                // 奖励元宝
                Integer bonusWood = 20; // TODO 暂时测试(元宝逻辑为随即获得）
                if (currencyService.canGive(player, Currency.GOLD, bonusWood, false)) {
                    currencyService.give(player, Currency.GOLD, bonusWood);
                }
                // TODO 奖励阅历 暂时不加 因为木有这个属性 啊哈哈
                Integer bonusSoul = 10;
                online.write(new S_C_RepPrizeInfoResp(bonusCoin, repTemplate.getBonusExp(), (byte) 100, bonusSoul, bonusWood));
            }
        }
    }

    public VisibleMonster getMonster(Integer monsterId) {
        return monsterManager.getMonster(monsterId);
    }

    public int aliveMonsterCount() {
        return monsterManager.aliveMonsterCount();
    }
}
