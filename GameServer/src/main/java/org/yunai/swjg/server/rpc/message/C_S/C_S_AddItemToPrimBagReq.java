package org.yunai.swjg.server.rpc.message.C_S;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【20803】: 道具拾取请求
 */
public class C_S_AddItemToPrimBagReq extends GameMessage {
    public static final short CODE = 20803;

    /**
     * 道具数量
     */
    private Byte count;
    /**
     * 道具模版编号
     */
    private Integer templateId;

    public C_S_AddItemToPrimBagReq() {
    }

    public C_S_AddItemToPrimBagReq(Byte count, Integer templateId) {
        this.count = count;
        this.templateId = templateId;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Byte getCount() {
		return count;
	}

	public void setCount(Byte count) {
		this.count = count;
	}
	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            C_S_AddItemToPrimBagReq struct = new C_S_AddItemToPrimBagReq();
            struct.setCount(byteArray.getByte());
            struct.setTemplateId(byteArray.getInt());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            C_S_AddItemToPrimBagReq struct = (C_S_AddItemToPrimBagReq) message;
            ByteArray byteArray = ByteArray.createNull(5);
            byteArray.create();
            byteArray.putByte(struct.getCount());
            byteArray.putInt(struct.getTemplateId());
            return byteArray;
        }
    }
}