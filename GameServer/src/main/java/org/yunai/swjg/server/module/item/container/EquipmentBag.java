package org.yunai.swjg.server.module.item.container;

import org.yunai.swjg.server.core.constants.SysMessageConstants;
import org.yunai.swjg.server.core.role.Role;
import org.yunai.swjg.server.module.item.ItemDef;
import org.yunai.swjg.server.module.item.template.EquipmentItemTemplate;
import org.yunai.swjg.server.module.item.vo.Item;
import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.swjg.server.rpc.message.S_C.S_C_SysMessageReq;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 装备背包
 * User: yunai
 * Date: 13-4-6
 * Time: 下午4:46
 */
public class EquipmentBag extends CommonBag {

    /**
     * 装备背包规定大小为6
     */
    private static final int SIZE = 6;
    /**
     * 装备类型与背包位置规定MAP
     */
    private static final Map<ItemDef.Type, Integer> typePositionMap = new HashMap<>(SIZE);
    static {
        int position = 0;
        typePositionMap.put(ItemDef.Type.WEAPON, ++position);
        typePositionMap.put(ItemDef.Type.CLOTHES, ++position);
        typePositionMap.put(ItemDef.Type.CAP, ++position);
        typePositionMap.put(ItemDef.Type.SHOES, ++position);
        typePositionMap.put(ItemDef.Type.AMULET, ++position);
        typePositionMap.put(ItemDef.Type.ACCESSORIES, ++position);
    }

    /**
     * 玩家信息
     */
    private final Player player;

    public EquipmentBag(Player player, Role owner) {
        super(owner, BagType.EQUIPMENT, SIZE);
        this.player = player;
    }

    @Override
    public void putItem(Item item) {
        super.putItem(item);

        // TODO 将装备放到equipMap，
        // TODO 等Template想好之后，在弄这里,因为Position靠Template来确定
    }

    /**
     * @param type 装备类型
     * @return 获得该装备类型的装备
     */
    public Item getEquipment(ItemDef.Type type) {
        return super.getByIndex(getPosition(type));
    }

    /**
     * @param type 装备类型
     * @return 获得装备类型的背包位置
     */
    public static int getPosition(ItemDef.Type type) {
        return typePositionMap.get(type);
    }

    public boolean equip(ShoulderBag fromBag, Item equipment) {
        // 检查是否在fromBag里
        if (fromBag.getByIndex(equipment.getIndex()) != equipment) {
            return false;
        }
        // 道具不是装备
        if (!equipment.isEquipment()) {
            player.message(new S_C_SysMessageReq(SysMessageConstants.ITEM_NOT_EQUIPMENT));
            return false;
        }
        // 职业限制
        EquipmentItemTemplate template = (EquipmentItemTemplate) equipment.getTemplate();
        if (!template.getVocations().contains(owner.getVocation())) {
            player.message(new S_C_SysMessageReq(SysMessageConstants.ITEM_EQUIP_VOCATION_ERROR));
            return false;
        }
        // 穿戴
        List<Item> changedItems = new ArrayList<>(2);
        Item curEquipment = getEquipment(equipment.getType());
        if (curEquipment != null) {
            curEquipment.changeOwner(Item.OWNER_ID_NONE);
            equipment.changeOwner(owner.getId());
            player.getInventory().swap(equipment, curEquipment);
            changedItems.add(equipment);
            changedItems.add(curEquipment);
        } else {
            Item msgDelItem = Item.buildRemoveItem(equipment);
            equipment.changeOwner(owner.getId());
            boolean rs = fromBag.move(equipment, this, getPosition(equipment.getType()));
            if (!rs) {
                player.message(new S_C_SysMessageReq(SysMessageConstants.ITEM_BAG_INDEX_NOT_EMPTY));
                return false;
            }
            changedItems.add(equipment);
            changedItems.add(msgDelItem);
        }
        // 重新计算属性
        owner.calcRoleProps(true);
        // 发送修改给客户端
        player.getInventory().sendModifyMessage(changedItems);
        return true;
    }

    public boolean unEquipment(Item equipment, ShoulderBag bag, int toBagIndex) {
        if (toBagIndex != Bag.BAG_INDEX_EMPTY) {
            Item item = bag.getByIndex(toBagIndex);
            if (item != null) {
                if (!item.isEquipment()) {
                    player.message(new S_C_SysMessageReq(SysMessageConstants.ITEM_NOT_EQUIPMENT));
                    return false;
                }
                if (equipment.getType() != item.getType()) {
                    player.message(new S_C_SysMessageReq(SysMessageConstants.ITEM_EQUIP_TYPE_ERROR));
                    return false;
                }
                return equip(bag, item);
            }
        } else {
            toBagIndex = bag.getFirstEmptyIndex();
        }
        Item msgDelItem = Item.buildRemoveItem(equipment);
        if (toBagIndex == BAG_INDEX_EMPTY || !move(equipment, bag, toBagIndex)) {
            player.message(new S_C_SysMessageReq(SysMessageConstants.ITEM_BAG_FULL));
            return false;
        }
        // 设置道具携带者为空
        equipment.changeOwner(Item.OWNER_ID_NONE);
        // 重新计算属性
        owner.calcRoleProps(true);
        // 发送修改给客户端
        List<Item> changeItems = new ArrayList<>(2);
        changeItems.add(equipment);
        changeItems.add(msgDelItem);
        player.getInventory().sendModifyMessage(changeItems);
        return true;
    }

    public boolean upEquip() {
        return true;
    }

    @Override
    public void onLoad() {
        // TODO 未实现
    }

    @Override
    public void onChanged() {
        // TODO 未实现
    }
}