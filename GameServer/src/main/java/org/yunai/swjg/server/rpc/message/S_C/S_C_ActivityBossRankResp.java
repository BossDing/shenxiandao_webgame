package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import java.util.List;
import org.yunai.swjg.server.rpc.struct.StActivityBossPlayerRank;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21612】: BOSS活动排行版信息
 */
public class S_C_ActivityBossRankResp extends GameMessage {
    public static final short CODE = 21612;

    /**
     * 玩家输出排行版数组
     */
    private List<StActivityBossPlayerRank> ranks;

    public S_C_ActivityBossRankResp() {
    }

    public S_C_ActivityBossRankResp(List<StActivityBossPlayerRank> ranks) {
        this.ranks = ranks;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public List<StActivityBossPlayerRank> getRanks() {
		return ranks;
	}

	public void setRanks(List<StActivityBossPlayerRank> ranks) {
		this.ranks = ranks;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_ActivityBossRankResp struct = new S_C_ActivityBossRankResp();
		struct.setRanks(getMessageList(StActivityBossPlayerRank.Decoder.getInstance(), byteArray, StActivityBossPlayerRank.class));
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_ActivityBossRankResp struct = (S_C_ActivityBossRankResp) message;
            ByteArray byteArray = ByteArray.createNull(0);
            byte[][] ranksBytes = convertMessageList(byteArray, StActivityBossPlayerRank.Encoder.getInstance(), struct.getRanks());
            byteArray.create();
            putMessageList(byteArray, ranksBytes);
            return byteArray;
        }
    }
}