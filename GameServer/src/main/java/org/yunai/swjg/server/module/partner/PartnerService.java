package org.yunai.swjg.server.module.partner;

import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import org.yunai.swjg.server.core.constants.SysMessageConstants;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.currency.Currency;
import org.yunai.swjg.server.module.currency.CurrencyService;
import org.yunai.swjg.server.module.formation.vo.Formation;
import org.yunai.swjg.server.module.partner.template.PartnerTemplate;
import org.yunai.swjg.server.module.partner.vo.Partner;
import org.yunai.swjg.server.module.player.template.ReputationTemplate;
import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.swjg.server.rpc.message.S_C.S_C_PartnerInnListResp;
import org.yunai.swjg.server.rpc.message.S_C.S_C_SysMessageReq;
import org.yunai.swjg.server.rpc.struct.StPartnerInnInfo;
import org.yunai.yfserver.common.LoggerFactory;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 伙伴Service
 * User: yunai
 * Date: 13-5-29
 * Time: 下午8:37
 */
@Service
public class PartnerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerFactory.Logger.partner, PartnerService.class);

    @Resource
    private CurrencyService currencyService;

    /**
     * 客栈中的宠物列表
     * TODO 暂时不考虑前置任务
     *
     * @param online 在线信息
     */
    public void innList(Online online) {
        Player player = online.getPlayer();
        PartnerDiary partnerDiary = online.getPlayer().getPartnerDiary();
        short reputationLevel = ReputationTemplate.getByReputation(player.getReputation()).getLevel();
        List<PartnerTemplate> templates = PartnerTemplate.getByReputationLevel(reputationLevel);
        List<StPartnerInnInfo> partnerInnInfos = new ArrayList<>(templates.size());
        for (PartnerTemplate template : templates) {
            Partner partner = partnerDiary.getByTemplateId(template.getId());
            if (partner == null) {
                partnerInnInfos.add(new StPartnerInnInfo((short) 1, template.getBaseMagic(), template.getReputationTemplate().getLevel(),
                        PartnerDef.Status.INN.get(), template.getBaseStrength(), template.getBaseStunt(), template.getId()));
            } else {
                partnerInnInfos.add(new StPartnerInnInfo(partner.getLevel(), partner.getMagic(), template.getReputationTemplate().getLevel(),
                        partner.getStatus().get(), partner.getStrength(), partner.getStunt(), template.getId()));
            }
        }
        online.write(new S_C_PartnerInnListResp(partnerInnInfos));
    }

    /**
     * 雇佣伙伴
     *
     * @param online     在线信息
     * @param templateId 伙伴模版编号
     */
    public void recruit(Online online, short templateId) {
        Player player = online.getPlayer();
        // 模版是否存在
        PartnerTemplate template = PartnerTemplate.get(templateId);
        if (template == null) {
            LOGGER.error("[recruit] [online({}) not found partner({})].", player.getId(), templateId);
            player.message(new S_C_SysMessageReq(SysMessageConstants.PARTNER_TEMPLATE_NOT_FOUND));
            return;
        }
        // 检查声望够不够
        if (template.getNeedReputation() > player.getReputation()) {
            player.message(new S_C_SysMessageReq(SysMessageConstants.PLAYER_REPUTATION_NOT_ENOUGH));
            return;
        }
        // 检查钱够不够
        if (currencyService.canCost(player, Currency.COIN, template.getNeedCoin(), true)) {
            return;
        }
        // 检查队伍空间是否足够
        Formation formation = player.getFormation();
        if (formation.isFull()) {
            player.message(new S_C_SysMessageReq(SysMessageConstants.FORMATION_OVER_SIZE));
            return;
        }
        // 扣除铜钱
        currencyService.cost(player, Currency.COIN, template.getNeedCoin());
        // 添加伙伴
        PartnerDiary partnerDiary = player.getPartnerDiary();
        partnerDiary.recruit(template);
    }

    /**
     * 解雇伙伴
     *
     * @param online 在线信息
     * @param id     伙伴编号
     */
    public void fire(Online online, int id) {
        PartnerDiary partnerDiary = online.getPlayer().getPartnerDiary();
        Partner partner = partnerDiary.get(id);
        if (partner == null) {
            online.write(new S_C_SysMessageReq(SysMessageConstants.PARTNER_NOT_FOUND));
            return;
        }
        partnerDiary.fire(partner);
    }
}