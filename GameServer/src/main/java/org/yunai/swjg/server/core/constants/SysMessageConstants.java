package org.yunai.swjg.server.core.constants;

/**
 * 系统消息枚举类型
 * User: yunai
 * Date: 13-4-28
 * Time: 下午11:30
 */
public class SysMessageConstants {

    // ===================================================================================================
    /**
     * 服务器即将关闭
     */
    public static final int SERVER_WILL_SHUTDOWN = 1;

    // [公用模块010 - 100]===================================================================================================
    /**
     * 角色不存在
     */
    public static final int COMMON_ROLE_NOT_FOUND = 10;
    // [道具模块101 - 200]===================================================================================================
    /**
     * 主背包空间不够
     */
    @Deprecated
    public static final int BAG_PRIM_NOT_ENOUGH = 101;
    /**
     * 背包已满 TODO 以后最好提示哪个背包满了。
     */
    public static final int ITEM_BAG_FULL = 102;
    /**
     * 道具数量不足
     */
    public static final int ITEM_NOT_ENOUGH = 103;
    /**
     * 道具背包类型错误
     */
    public static final int ITEM_BAG_TYPE_ERROR = 104;
    /**
     * 道具不存在
     */
    public static final int ITEM_NOT_FOUND = 105;
    /**
     * 道具不能使用
     */
    public static final int ITEM_CAN_NOT_USE = 106;
    /**
     * 道具使用等级不足
     */
    public static final int ITEM_USE_LEVEL_NOT_ENOUGH = 107;
    /**
     * 穿戴时，职业错误
     */
    public static final int ITEM_EQUIP_VOCATION_ERROR = 108;
    /**
     * 道具不是装备
     */
    public static final int ITEM_NOT_EQUIPMENT = 109;
    /**
     * 道具移动到背包某个位置时，该位置已经放有物品
     */
    public static final int ITEM_BAG_INDEX_NOT_EMPTY = 110;
    /**
     * 道具装备类型不对
     */
    public static final int ITEM_EQUIP_TYPE_ERROR = 111;
    /**
     * 道具装备位置不对
     */
    public static final int ITEM_EQUIP_POSITION_ERROR = 112;
    /**
     * 道具移动失败
     */
    public static final int ITEM_MOVE_FAIL = 113;

    // [任务模块201 - 300]===================================================================================================
    /**
     * 任务接受等级不够
     */
    public static final int QUEST_ACCEPT_LEVEL_NOT_ENOUGH = 201;
    /**
     * 任务正在进行中，无法接受该任务
     */
    public static final int QUEST_ACCEPT_DOING = 202;
    /**
     * 任务接受前置任务未完成
     */
    public static final int QUEST_ACCEPT_PRE_QUEST_NOT_FINISHED = 203;
    /**
     * 任务接受时，该任务已经完成
     */
    public static final int QUEST_ACCEPT_FINISHED = 204;
    /**
     * 任务取消时，该任务未处于进行中状态
     */
    public static final int QUEST_CANCEL_NOT_DOING = 205;
    /**
     * 任务完成时，该任务完成条件未满足
     */
    @Deprecated
    public static final int QUEST_FINISH_CONDITION_NOT_MEET = 206;
    /**
     * 任务完成时，该任务不是在进行中状态
     */
    public static final int QUEST_FINISH_NOT_DOING = 207;

    // [怪物模块301 - 400]===================================================================================================
    /**
     * 怪物已经死亡
     */
    public static final int MONSTER_DEAD = 301;
    /**
     * 怪物不存在
     */
    public static final int MONSTER_NOT_FOUND = 302;

    // [活动模块401 - 500]===================================================================================================
    /**
     * BOSS活动已结束
     */
    public static final int ACTIVITY_BOSS_END = 401;
    /**
     * BOSS活动玩家死亡状态
     */
    public static final int ACTIVITY_BOSS_PLAYER_DEAD = 402;

    // [伙伴模块501 - 600]===================================================================================================
    /**
     * 伙伴模版未找到
     */
    public static final int PARTNER_TEMPLATE_NOT_FOUND = 501;
    /**
     * 伙伴不存在
     */
    public static final int PARTNER_NOT_FOUND = 502;
    /**
     * 伙伴处于解雇状态
     */
    public static final int PARTNER_STATUS_IN_FIRE = 503;

    // [玩家模块601 - 700]===================================================================================================
    /**
     * 玩家货币[元宝]超过上限
     */
    public static final int PLAYER_CURRENCY_GOLD_OVER_MAX = 601;
    /**
     * 玩家货币[铜钱]超过上限
     */
    public static final int PLAYER_CURRENCY_COIN_OVER_MAX = 602;
    /**
     * 玩家货币[元宝]不足
     */
    public static final int PLAYER_CURRENCY_GOLD_NOT_ENOUGH = 603;
    /**
     * 玩家货币[铜钱]不足
     */
    public static final int PLAYER_CURRENCY_COIN_NOT_ENOUGH = 604;
    /**
     * 玩家声望不足
     */
    public static final int PLAYER_REPUTATION_NOT_ENOUGH = 605;
    /**
     * 玩家丹药已经服用满
     */
    public static final int PLAYER_MEDICINE_PROP_FULL = 606;
    // [阵形模块701 - 800]===================================================================================================
    /**
     * 阵形人数超过上限
     */
    public static final int FORMATION_OVER_SIZE = 701;
}
