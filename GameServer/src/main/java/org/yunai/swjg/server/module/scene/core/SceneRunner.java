package org.yunai.swjg.server.module.scene.core;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.yunai.yfserver.common.LoggerFactory;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;

/**
 * 场景Runner
 * User: yunai
 * Date: 13-4-10
 * Time: 下午1:59
 */
public class SceneRunner<T extends AbstractScene> implements Callable<Integer> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerFactory.Logger.scene, SceneRunner.class);

    /**
     * 绑定的场景
     */
    private final T scene;
    /**
     * 执行中的Future
     */
    private Future<Integer> future;

    public SceneRunner(T scene) {
        this.scene = scene;
    }

    @Override
    public Integer call() throws Exception {
        try {
            scene.tick();
        } catch (Exception e) {
            LOGGER.error("[call] [error:{}]", ExceptionUtils.getStackTrace(e));
        }
        return 0;
    }

    public T getScene() {
        return scene;
    }

    public void setFuture(Future<Integer> future) {
        this.future = future;
    }

    /**
     * 任务({@link #future}是否执行完
     *
     * @return true/false
     */
    public boolean isDone() {
        return future == null || future.isDone();
    }
}
