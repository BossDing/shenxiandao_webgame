package org.yunai.swjg.server.core;

import org.yunai.swjg.server.core.service.GameMessageProcessor;
import org.yunai.swjg.server.module.activity.ActivityDef;
import org.yunai.swjg.server.module.activity.msg.SysActivityChangeStatusMessage;
import org.yunai.yfserver.spring.BeanManager;
import org.yunai.yfserver.util.StringUtils;

import java.util.Scanner;

/**
 * 输入线程
 * User: yunai
 * Date: 13-4-27
 * Time: 下午9:59
 */
public class ScannerThread extends Thread {

    private static GameMessageProcessor gameMessageProcessor;
    static {
        gameMessageProcessor = BeanManager.getBean(GameMessageProcessor.class);
    }

    @SuppressWarnings("InfiniteLoopStatement")
    @Override
    public void run() {
System.err.println("[ScannerThread start].");
        Scanner s = new Scanner(System.in);
        while (true) {
            if (s.hasNext()) {
                String[] inputs = StringUtils.split(s.nextLine(), "\\|");
                for (String input : inputs) {
                    handleInput(input);
                }
            }
        }
    }

    private void handleInput(String input) {
        switch (input) {
            case "exit":
                System.exit(0);
                break;
            case "boss_activity_a_prepare":
                gameMessageProcessor.put(new SysActivityChangeStatusMessage(1, ActivityDef.Status.PREPARE));
                break;
            case "boss_activity_a_process":
                gameMessageProcessor.put(new SysActivityChangeStatusMessage(1, ActivityDef.Status.PROCESS));
                break;
            case "boss_activity_a_end":
                gameMessageProcessor.put(new SysActivityChangeStatusMessage(1, ActivityDef.Status.END));
                break;
            default:
                System.err.println("[ScannerThread]: 异常输入.");
        }
    }
}

// boss_activity_a_prepare|boss_activity_a_process|boss_activity_a_end