package org.yunai.swjg.server.core.constants;

/**
 * 场景枚举
 * User: yunai
 * Date: 13-5-21
 * Time: 上午11:15
 */
public class SceneConstants {

    /**
     * 场景编号 - 赤炎兽
     */
    public static final int SCENE_BOSS_ACTIVITY_A = 10000;

    /**
     * 默认复活场景编号X
     */
    public static final short RELIVE_SCENE_X_DEFAULT = 1;
    /**
     * 默认复活场景编号Y
     */
    public static final short RELIVE_SCENE_Y_DEFAULT = 1;
}
