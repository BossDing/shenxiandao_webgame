package org.yunai.swjg.server.module.activity.heatbeat;

import org.yunai.swjg.server.core.config.SharedConstants;
import org.yunai.swjg.server.core.heartbeat.HeartbeatTask;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.rep.ActivityRepScene;
import org.yunai.swjg.server.rpc.message.S_C.S_C_ActivityBossCanMoveResp;
import org.yunai.yfserver.spring.BeanManager;
import org.yunai.yfserver.time.TimeService;

import java.util.Iterator;
import java.util.Map;

/**
 * Boss活动复活任务
 * User: yunai
 * Date: 13-5-24
 * Time: 上午10:29
 */
public class ActivityBossReviveTask implements HeartbeatTask {

    private static TimeService timeService;
    static {
        timeService = BeanManager.getBean(TimeService.class);
    }

    /**
     * 玩家死亡时间Map<br />
     * KEY: 玩家编号<br />
     * VALUE: 复活时间
     */
    private final Map<Integer, Long> playerReviveTimes;
    /**
     * 活动副本
     */
    private final ActivityRepScene rep;

    public ActivityBossReviveTask(ActivityRepScene rep, Map<Integer, Long> playerReviveTimes) {
        this.rep = rep;
        this.playerReviveTimes = playerReviveTimes;
    }

    @Override
    public long period() {
        return SharedConstants.GS_HEART_BEAT_INTERVAL;
    }

    @Override
    public void run() {
        if (playerReviveTimes.size() == 0) {
            return;
        }
        Iterator<Map.Entry<Integer, Long>> iterator = playerReviveTimes.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Integer, Long> playerDeadTime = iterator.next();
            if (!timeService.timeUp(playerDeadTime.getValue())) {
                continue;
            }
            iterator.remove();
            Online online = rep.getOnline(playerDeadTime.getKey());
            if (online == null) {
                continue;
            }
            online.write(new S_C_ActivityBossCanMoveResp());
        }
    }

}
