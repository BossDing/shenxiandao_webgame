package org.yunai.swjg.server.module.monster.vo;

import org.yunai.swjg.server.module.idSequence.IdSequenceHolder;
import org.yunai.swjg.server.module.monster.template.VisibleMonsterTemplate;
import org.yunai.swjg.server.module.rep.RepScene;
import org.yunai.swjg.server.rpc.struct.StVisibleMonsterInfo;
import org.yunai.yfserver.util.MathUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 可见怪物组
 * User: yunai
 * Date: 13-5-17
 * Time: 下午3:12
 */
public class VisibleMonster {

    /**
     * 编号
     */
    private final Integer id;
    /**
     * 是否存活
     */
    private boolean alive;
    /**
     * 模版
     */
    private final VisibleMonsterTemplate template;
    /**
     * 副本
     */
    private final RepScene scene;
    /**
     * 怪物数组
     */
    private final List<Monster> monsters;

    public VisibleMonster(VisibleMonsterTemplate template, RepScene scene) {
        this.id = IdSequenceHolder.genMonsterId();
        this.alive = true;
        this.template = template;
        this.scene = scene;
        // 初始化怪物
        this.monsters = new ArrayList<>(template.getMonsterGroups().size());
        for (VisibleMonsterTemplate.MonsterGroup monsterGroup : template.getMonsterGroups()) {
            monsters.add(new Monster(monsterGroup.getTemplate(), monsterGroup.getPosition()));
        }
    }

    public Integer getId() {
        return id;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public VisibleMonsterTemplate getTemplate() {
        return template;
    }

    public RepScene getScene() {
        return scene;
    }

    public List<Monster> getMonsters() {
        return monsters;
    }

    // ==================== 非get/set方法 ====================
    public Double getHpMax() {
        Double hpMax = 0D;
        for (Monster monster : monsters) {
            hpMax += monster.getHpMax();
        }
        return hpMax;
    }

    public Double getHpCur() {
        Double hpCur = 0D;
        for (Monster monster : monsters) {
            hpCur += monster.getHpCur();
        }
        return hpCur;
    }

    public StVisibleMonsterInfo genStVisibleMonsterInfo() {
        return new StVisibleMonsterInfo(MathUtils.ceil2Int(getHpCur()), MathUtils.ceil2Int(getHpMax()),
                id, template.getId());
    }

    public int getLevel() {
        int level = 0;
        for (Monster monster : monsters) {
            level += monster.getLevel();
        }
        return MathUtils.ceil2Int(level * 1.0 / monsters.size());
    }

//    public void endBattle(boolean attWin) {
//        if (attWin) {
//            setAlive(false);
//            scene.onMonsterDead(this);
//        }
//    }
}
