package org.yunai.swjg.server.module.activity.msg;

import org.yunai.swjg.server.module.activity.AbstractActivity;
import org.yunai.swjg.server.module.activity.ActivityDef;
import org.yunai.swjg.server.module.activity.ActivityService;
import org.yunai.yfserver.message.schedule.ScheduledCronMessage;
import org.yunai.yfserver.spring.BeanManager;

/**
 * 改变活动状态定时消息
 * User: yunai
 * Date: 13-5-25
 * Time: 上午1:51
 */
public class ScheduleCronActivityChangeStatusMessage
        extends ScheduledCronMessage {

    private static ActivityService activityService;
    static {
        activityService = BeanManager.getBean(ActivityService.class);
    }

    /**
     * 活动
     */
    private final AbstractActivity activity;
    /**
     * 目标活动状态
     */
    private final ActivityDef.Status status;

    public ScheduleCronActivityChangeStatusMessage(AbstractActivity activity, ActivityDef.Status status, String cron) {
        super(cron);
        this.activity = activity;
        this.status = status;
    }

    @Override
    public void executeImpl() {
        activityService.changeStatus(activity.getId(), status);
    }

    @Override
    public short getCode() {
        return 0;
    }
}
