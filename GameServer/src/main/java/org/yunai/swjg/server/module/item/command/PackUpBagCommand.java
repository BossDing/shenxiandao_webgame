package org.yunai.swjg.server.module.item.command;

import org.springframework.stereotype.Controller;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.item.ItemService;
import org.yunai.swjg.server.rpc.message.C_S.C_S_PackUpBagReq;

import javax.annotation.Resource;

/**
 * 整理背包命令
 * User: yunai
 * Date: 13-4-12
 * Time: 下午1:41
 */
@Controller
public class PackUpBagCommand extends GameMessageCommand<C_S_PackUpBagReq> {

    @Resource
    private ItemService itemService;

    @Override
    public void execute(Online online, C_S_PackUpBagReq msg) {
        itemService.packUpBag(online, msg.getBagId());
    }
}