package org.yunai.swjg.server.core.heartbeat;

/**
 * 心跳计时器
 * User: yunai
 * Date: 13-5-24
 * Time: 上午9:57
 */
public interface HeartbeatTimer {

    /**
     * @return 是否到时间
     */
    boolean timeUp();

    /**
     * 重置，进入下一轮计时
     */
    void nextRound();
}
