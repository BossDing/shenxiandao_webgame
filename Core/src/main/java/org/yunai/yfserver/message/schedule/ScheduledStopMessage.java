package org.yunai.yfserver.message.schedule;

import org.yunai.yfserver.message.MessageType;

/**
 * 取消某个任务
 * TODO 暂时没用处
 * User: yunai
 * Date: 13-3-28
 * Time: 下午5:20
 */
public class ScheduledStopMessage extends ScheduledMessage {

    private ScheduledMessage msg;

    protected ScheduledStopMessage(long createTimestamp, ScheduledMessage msg) {
        super(createTimestamp);
        this.msg = msg;
    }

    @Override
    public short getCode() {
        return MessageType.SCHD_STOP;
    }

    @Override
    public void execute() {
        msg.cancel();
    }
}
