package org.yunai.yfserver.annotation;

import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;

/**
 * 线程不安全方法注解<br />
 * 当方法被该注解注释后，请保证方法不会出现线程同时调用的情况
 * User: yunai
 * Date: 13-4-27
 * Time: 下午5:20
 */
@Target(METHOD)
public @interface ThreadNotSafe {
}
