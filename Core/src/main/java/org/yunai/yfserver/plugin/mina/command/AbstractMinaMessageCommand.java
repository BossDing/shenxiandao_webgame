package org.yunai.yfserver.plugin.mina.command;

import org.yunai.yfserver.command.Command;
import org.yunai.yfserver.plugin.mina.message.AbstractMinaMessage;
import org.yunai.yfserver.plugin.mina.session.AbstractMinaSession;

/**
 * Mina消息命令基类
 * User: yunai
 * Date: 13-3-26
 * Time: 下午12:50
 */
public abstract class AbstractMinaMessageCommand<S extends AbstractMinaSession, M extends AbstractMinaMessage>
        implements Command {

    public abstract void execute(S session, M msg);
}
