package org.yunai.yfserver.plugin.mina.message;

import org.yunai.yfserver.message.ISessionMessage;
import org.yunai.yfserver.plugin.mina.session.AbstractMinaSession;

/**
 * Mina消息基类
 * User: yunai
 * Date: 13-3-26
 * Time: 上午12:10
 */
public abstract class AbstractMinaMessage<T extends AbstractMinaSession> implements ISessionMessage<T> {

    private T session;

    @Override
    public T getSession() {
        return session;
    }

    @Override
    public void setSession(T session) {
        this.session = session;
    }

}
