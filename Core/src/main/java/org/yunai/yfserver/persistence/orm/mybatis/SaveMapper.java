package org.yunai.yfserver.persistence.orm.mybatis;

import org.yunai.yfserver.persistence.orm.Entity;

/**
 * 保存操作Mapper接口。包含insert + update操作<br />
 * @see org.yunai.yfserver.persistence.updater.PersistenceObjectUpdater
 * User: yunai
 * Date: 13-4-9
 * Time: 下午4:49
 */
public interface SaveMapper<T extends Entity> {

    /**
     * 插入数据
     * @param entity 数据
     */
    void insert(T entity);

    /**
     * 更新数据
     * @param entity 数据
     */
    void update(T entity);
}
