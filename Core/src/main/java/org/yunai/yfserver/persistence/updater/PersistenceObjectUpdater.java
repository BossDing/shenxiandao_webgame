package org.yunai.yfserver.persistence.updater;

import org.yunai.yfserver.persistence.PersistenceObject;

/**
 * 可持久化业务对象更新器
 * User: yunai
 * Date: 13-4-9
 * Time: 下午4:20
 */
public interface PersistenceObjectUpdater {

    /**
     * 保存对象
     * @param obj 对象
     */
    void save(PersistenceObject<?, ?> obj);

    /**
     * 删除对象
     * @param obj 对象
     */
    void delete(PersistenceObject<?, ?> obj);
}