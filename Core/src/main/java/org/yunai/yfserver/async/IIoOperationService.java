package org.yunai.yfserver.async;

/**
 * IO操作服务接口
 * User: yunai
 * Date: 13-3-28
 * Time: 下午2:20
 */
public interface IIoOperationService {

    /**
     * 异步执行操作
     *
     * @param operation 操作
     */
    void asyncExecute(IIoOperation operation);

    /**
     * 同步执行操作
     *
     * @param operation 操作
     */
    void syncExecute(IIoOperation operation);

//    /**
//     * 同步串行执行操作
//     *
//     * @param operation 操作
//     * @param key 发起者ID
//     */
//    void asyncExecute(IIoOperation operation, String key);

    /**
     * 通知服务
     */
    void stop();

    /**
     * 同步串行执行操作
     *
     * @param operation 操作
     */
    void asyncExecute(IIoSerialOperation operation);
}
