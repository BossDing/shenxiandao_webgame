package org.yunai.yfserver.async;

/**
 * IoOperation例子
 * User: yunai
 * Date: 13-3-28
 * Time: 下午2:57
 */
public class DemoIoOperation implements IIoOperation {

//    private volatile boolean succes = false;

    @Override
    public State doStart() {
        System.out.println(Thread.currentThread() + ": doStart!");
        return State.STARTED;
    }

    @Override
    public State doIo() {
        System.out.println(Thread.currentThread() + ": doIo!");
        return State.IO_DONE;
    }

    @Override
    public State doFinish() {
        System.out.println(Thread.currentThread() + ": doFinish!");
        return State.FINISHED;
    }
}
